#include <bits/stdc++.h>

using namespace std;

long long ans = 0;
int last = -1;

void dfs(int sum, vector<bool>& vis, vector<int> st) {
  if (count(vis.begin(), vis.end(), false) == 0) {
    if (last + 1 != vis.size()) ans++;
    return;
  }
  if (!vis.back()) {
    if (vis.size() > st[1] && count(vis.begin(), vis.begin() + st[1], false) == 0) {
      return;
    }
  }
  for (int i = 0; i < vis.size(); i++) {
    if (!vis[i]) {
      vis[i] = true;
      last = i;
      int t = count(vis.begin(), vis.begin() + i, false);
      if (t <= sum) {
        bool ok = false;
        auto vec = st;
        if (i < vec[0]) {
          vec[0] = i;
          ok = true;
        } else if (i < vec[1]) {
          vec[1] = i;
          ok = true;
        } else if (count(vis.begin() + vec[1], vis.begin() + i, false) == 0) {
          ok = true;
        }
        if (ok) {
          dfs(sum - t, vis, vec);
        }
      }
      vis[i] = false;
    }
  }
}

const int mod = 1e9 + 7;

long long solve(long long n, int m) {
  long long ret = n + 1;
  for (int i = 2; i <= 45; i++) {
    ans = 0;
    vector<bool> vis(i);
    last = -1111;
    dfs(m, vis, vector<int>(2, 123456));
    if (n >= i) {
      ret = (ret + (n - i + 1) % mod * ans) % mod;
      cout << ret << endl;
    }
    cout << i << ": " << ans << endl;
  }
  return ret;
}

int main(int argc, char *argv[]) {
  //cout << solve(4, 5) << endl;
  //cout << solve(10, 25) << endl;
  cout << solve(1000000000000000000LL, 40) << endl;
  return 0;
}
