#include <bits/stdc++.h>
#include "../Library/misc/fraction.hpp"
#include "../Library/geometry/point.hpp"

using namespace std;

typedef Point<long long> P;

int main(int argc, char *argv[]) {
  auto solve = [](int n) {
    vector<long long> S(1, 290797);
    while (S.size() < 2 * n + 1) {
      S.push_back(S.back() * S.back() % 50515093);
    }
    for (auto& x : S) {
      x = x % 2000 - 1000;
    }
    vector<P> a;
    for (int i = 1; i <= n; i++) {
      a.push_back(P(S[2 * i - 1], S[2 * i]));
    }
    map<Fraction, vector<pair<P, P>>> mp;
    vector<P> b;
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
        auto p = a[j] - a[i];
        Fraction frac(p.y, p.x);
        mp[frac].emplace_back(a[i], a[j]);
      }
    }
    int sum = 0;
    long long ans = 0;
    for (auto [k, v] : mp) {
      int now;
      if (k.b == 0) {
        set<long long> se;
        for (auto e : v) {
          se.insert(e.first.x);
        }
        now = se.size();
      } else {
        set<Fraction> se;
        for (auto [p1, p2] : v) {
          se.insert(Fraction((p1.y - p2.y) * (p2.x - 12345), p2.x - p1.x) + p2.y);
        }
        now = se.size();
      }
      ans += 2LL * now * sum;
      sum += now;
    }
    cout << sum << endl;
    return ans;
  };
  cout << solve(2500) << endl;
  return 0;
}
