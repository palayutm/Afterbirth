#ifndef SEGMENT_DISTANCE_H
#define SEGMENT_DISTANCE_H

#include "point.hpp"

typedef Point<double> P;
double seg_dist(P &s, P &e, P &p) {
  if (s == e) return (p - s).dist();
  auto d = (e - s).dist2(), t = min(d, max(.0, (p - s).dot(e - s)));
  return ((p - s) * d - (e - s) * t).dist() / d;
}

#endif /* SEGMENT_DISTANCE_H */
