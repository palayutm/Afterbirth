#include "fenwick_tree.hpp"
#include <gtest/gtest.h>

TEST(FenwickTree, FenwickTree) {
  FenwickTree<int> ft(5);
  ASSERT_EQ(0, ft.query(4));
  ft.add(1, 1);
  ASSERT_EQ(1, ft.query(4));
  ft.add(3, 1);
  ASSERT_EQ(2, ft.query(4));
  ASSERT_EQ(1, ft.query(2));
}
