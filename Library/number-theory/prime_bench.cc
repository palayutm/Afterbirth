#include "misc/random_util.hpp"
#include "prime.hpp"
#include <benchmark/benchmark.h>

static void BM_PrimeSieve(benchmark::State &state) {
  for (auto _ : state) {
    auto ret = prime::prime_table(state.range(0));
  }
}

static void BM_ProbablePrime(benchmark::State &state) {
  for (auto _ : state) {
    long long n = misc::rand_ll(1e17, 1e18);
    prime::probable_prime(n);
  }
}

BENCHMARK(BM_PrimeSieve)->Range(8 << 10, 1 << 27)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_ProbablePrime);
BENCHMARK_MAIN();
