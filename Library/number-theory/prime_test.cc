#include "misc/random_util.hpp"
#include "prime.hpp"
#include <gtest/gtest.h>

TEST(Prime, 1_to_1000000) {
  auto [is_prime, prime_list] = prime::prime_table(1000001);
  for (int i = 1; i <= 1000000; i++) {
    ASSERT_EQ(is_prime[i], prime::is_prime(i));
  }
}

TEST(Prime, probable_prime_1_to_100000) {
  auto [is_prime, prime_list] = prime::prime_table(100001);
  for (int i = 1; i <= 100000; i++) {
    ASSERT_EQ(is_prime[i], prime::probable_prime(i));
  }
}

TEST(Prime, probable_prime_1e11_to_1e12) {
  for (int i = 0; i < 5000; i++) {
    long long n = misc::rand_ll(1e11, 1e12);
    ASSERT_LE(1e11, n);
    ASSERT_LT(n, 1e12);
    ASSERT_EQ(prime::is_prime(n), prime::probable_prime(n));
  }
}

TEST(Prime, probable_prime_1e14_to_1e15) {
  for (int i = 0; i < 200; i++) {
    long long n = misc::rand_ll(1e14, 1e15);
    ASSERT_LE(1e14, n);
    ASSERT_LT(n, 1e15);
    ASSERT_EQ(prime::is_prime(n), prime::probable_prime(n));
  }
}

TEST(Prime, probable_prime_1e14_to_1e15_2) {
  for (int i = 0; i < 300; i++) {
    long long n = misc::rand_ll(1e14, 1e15);
    ASSERT_LE(1e14, n);
    ASSERT_LT(n, 1e15);
    if (prime::probable_prime(n)) {
      ASSERT_TRUE(prime::is_prime(n));
    }
  }
}
