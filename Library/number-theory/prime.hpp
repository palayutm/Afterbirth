#ifndef PRIME_H
#define PRIME_H

#include "misc/integer_util.hpp"
#include <algorithm>
#include <vector>

namespace prime {
using misc::fast_pow;
bool is_prime(long long n) {
  if (n == 1) {
    return false;
  }
  for (long long i = 2; i * i <= n; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

std::pair<std::vector<bool>, std::vector<int>> prime_table(int n) {
  std::vector<bool> is_prime(n, true);
  is_prime[0] = is_prime[1] = false;
  std::vector<int> prime_list;
  for (int i = 2; i < n; i++) {
    if (is_prime[i]) {
      prime_list.push_back(i);
    }
    for (int j = 0; j < prime_list.size() && i * prime_list[j] < n; j++) {
      is_prime[i * prime_list[j]] = false;
      if (i % prime_list[j] == 0) {
        break;
      }
    }
  }
  return {is_prime, prime_list};
}

/**
 * miller-rabin method
 */
bool probable_prime(long long n) {
  static const int S = 6;
  if (n < 100)
    return is_prime(n);
  if (!(n & 1))
    return false;
  int t = 0;
  long long u = n - 1;
  for (; !(u & 1); t++, u >>= 1)
    ;
  for (int i = 0; i < S; i++) {
    long long a = rand() % (n - 1) + 1;
    long long x = fast_pow(a, u, n);
    for (int j = 0; j < t; j++) {
      long long y = fast_pow(x, 2LL, n);
      if (y == 1 && x != 1 && x != n - 1) {
        return false;
      }
      x = y;
    }
    if (x != 1)
      return false;
  }
  return true;
}
} // namespace prime

#endif /* PRIME_H */
