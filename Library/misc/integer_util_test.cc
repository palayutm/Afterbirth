#include "integer_util.hpp"
#include <gtest/gtest.h>

TEST(int_pow, int_pow) {
  ASSERT_EQ(misc::fast_pow(2, 2, 3), 1);
  ASSERT_EQ(misc::fast_pow(5, 5, 7), 3);
  ASSERT_EQ(misc::fast_pow(1, 0, 10), 1);
}

TEST(long_pow, long_pow) {
  ASSERT_EQ(misc::fast_pow(123456789123456789LL, 123422211212323123LL,
                           1212323123123123129LL),
            946255076325465905LL);
  ASSERT_EQ(misc::fast_pow(1LL, 0LL, 100000000000000000LL), 1);
}
