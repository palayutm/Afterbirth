#include "random_util.hpp"
#include <benchmark/benchmark.h>

static void BM_RandomInt(benchmark::State &state) {
  for (auto _ : state) {
    misc::rand_int(10, 100);
  }
}

BENCHMARK(BM_RandomInt);
BENCHMARK_MAIN();
