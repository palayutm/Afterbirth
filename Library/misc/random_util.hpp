#ifndef RANDOM_UTIL_H
#define RANDOM_UTIL_H

#include <random>

namespace misc {
std::mt19937 gen((std::random_device())());
std::uniform_int_distribution<int> uid(0, (1LL << 31) - 1);
std::uniform_int_distribution<long long> uidll(0, (1LL << 62));

int rand_int(int L = 0, int R = (1 << 30)) { return uid(gen) % (R - L) + L; }

std::vector<int> rand_vector(int n, int L = 0, int R = 2e9) {
  std::vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    a[i] = rand_int(L, R);
  }
  return a;
}

long long rand_ll(long long L = 0, long long R = (1LL << 62)) {
  return uidll(gen) % (R - L) + L;
}

} // namespace misc

#endif /* RANDOM_UTIL_H */
