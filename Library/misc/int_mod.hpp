#ifndef INT_MOD_H
#define INT_MOD_H

#include "integer_util.hpp"
using misc::inverse;

struct IntMod {
  const static int mod = 1e9 + 7;
  int val;
  IntMod(int val = 0) : val(val) {}

  IntMod operator+(const IntMod &o) const {
    int v = val + o.val;
    if (v >= mod) v -= mod;
    return IntMod(v);
  }

  IntMod operator+=(const IntMod &o) {
    *this = *this + o;
    return *this;
  }

  IntMod operator-(const IntMod &o) const {
    int v = val - o.val;
    if (v < mod) v += mod;
    return IntMod(v);
  }

  IntMod operator-=(const IntMod &o) {
    *this = *this - o;
    return *this;
  }

  IntMod operator*(const IntMod &o) const {
    return IntMod(1LL * val * o.val % mod);
  }

  IntMod operator*=(const IntMod &o) {
    *this = *this * o;
    return *this;
  }

  IntMod operator/(const IntMod &o) const {
    return IntMod(1LL * val * inverse(o.val, mod) % mod);
  }

  IntMod operator/=(const IntMod &o) {
    *this = *this / o;
    return *this;
  }
};

#endif /* INT_MOD_H */
