#ifndef FRACTION_H
#define FRACTION_H

#include <numeric>
#include <cstdlib>

struct Fraction {
  long long a, b;
  int sign;

  Fraction() : a(0), b(0), sign(1) {}
  Fraction(long long x, long long y = 1) { set(x, y); }

  long long nt() const { return a * sign; }
  long long dt() const { return b; }

  void set(long long x, long long y) {
    if (x == 0) y = 1;
    else if (y == 0) x = 1;
    sign = (x * y < 0 ? -1 : 1);
    a = abs(x), b = abs(y);
    //reduction();
  }

  void reduction() {
    long long g = std::gcd(a, b);
    a /= g, b /= g;
  }

  bool operator<(const Fraction &o) const {
    return nt() * o.dt() < dt() * o.nt();
  }

  bool operator==(const Fraction &o) const {
    return !(*this < o) && !(o < *this);
  }

  Fraction operator-() const { return Fraction(-1 * nt(), dt()); };

  Fraction operator+(const Fraction &o) const {
    return Fraction(nt() * o.dt() + o.nt() * dt(), dt() * o.dt());
  }

  Fraction operator-(const Fraction &o) const {
    return Fraction(nt() * o.dt() - o.nt() * dt(), dt() * o.dt());
  }

  Fraction operator*(const Fraction &o) const {
    return Fraction(nt() * o.nt(), dt() * o.dt());
  }

  Fraction operator/(const Fraction &o) const {
    return Fraction(nt() * o.dt(), dt() * o.nt());
  }
};

#endif /* FRACTION_H */
