#include "fraction.hpp"
#include <gtest/gtest.h>

TEST(fraction, compare) {
  Fraction a(1, 2), b(1, 3);
  ASSERT_TRUE(b < a);
  ASSERT_TRUE(-a < -b);
  ASSERT_TRUE(-a < b);
  ASSERT_TRUE(-b < a);
  ASSERT_FALSE(a < a);
  ASSERT_TRUE(-a < a);
  ASSERT_TRUE(a == a);
  ASSERT_TRUE(a == -(-a));
}

TEST(fraction, operation) {
  Fraction a(-3, 4), b(1, 6);
  ASSERT_TRUE(a + b == Fraction(7, -12));
  ASSERT_TRUE(a + 3 == Fraction(-9, -4));
  ASSERT_TRUE(a - b == Fraction(-11, 12));
  ASSERT_TRUE(a * b == Fraction(3, -24));
  ASSERT_TRUE(a / b == Fraction(-9, 2));
  ASSERT_TRUE(a / b == Fraction(18, -4));
}
