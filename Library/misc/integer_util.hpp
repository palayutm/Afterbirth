#ifndef INTEGER_H
#define INTEGER_H

#include <vector>
#include <cassert>

namespace misc {
/**
 * return x * y % z
 */
long long fast_mul(long long x, long long y, long long z) {
  long long ret = 0;
  x %= z;
  for (; y; y >>= 1, x = x * 2 % z) {
    if (y & 1)
      ret = (ret + x) % z;
  }
  return ret;
}

long long fast_pow(long long x, long long y, long long z) {
  long long ret = 1;
  for (; y; y >>= 1, x = fast_mul(x, x, z)) {
    if (y & 1)
      ret = fast_mul(ret, x, z);
  }
  return ret;
}

int fast_pow(int x, int y, int z) {
  long long ret = 1;
  for (; y; y >>= 1, x = (long long)x * x % z) {
    if (y & 1)
      ret = ret * x % z;
  }
  return ret;
}

int inverse(int a, int md) {
  return a == 1 ? a : (long long)(md - md / a) * inverse(md % a, md) % md;
}

int inverse_non_prime(int a, int md) {
  a %= md;
  if (a < 0) a += md;
  int b = md, u = 0, v = 1;
  while (a) {
    int t = b / a;
    b -= t * a; std::swap(a, b);
    u -= t * v; std::swap(u, v);
  }
  assert(b == 1);
  if (u < 0) u += md;
  return u;
}

std::vector<int> inverse_table(int n, int md) {
  std::vector<int> inv(n);
  inv[1] = 1;
  for (int i = 2; i < n; ++i) {
    inv[i] = (long long)(md - md / i) * inv[md % i] % md;
  }
  return inv;
}
} // namespace integer

#endif /* INTEGER_H */
