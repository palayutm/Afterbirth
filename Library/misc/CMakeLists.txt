add_executable(random_util_test random_util_test.cc random_util.hpp)
target_link_libraries(random_util_test gtest_main)
add_test(random_util_test random_util_test)

add_executable(integer_util_test integer_util_test.cc integer_util.hpp)
target_link_libraries(integer_util_test gtest_main)
add_test(integer_util_test integer_util_test)

add_executable(random_util_bench random_util_bench.cc)
target_link_libraries(random_util_bench benchmark)
add_test(random_util_bench random_util_bench)

add_executable(fraction_test fraction_test.cc fraction.hpp)
target_link_libraries(fraction_test gtest_main)
add_test(fraction_test fraction_test)
