#include "shortest_path.hpp"
#include <gmock/gmock.h>

TEST(ShortestPath, SPFA) {
  graph::Graph g(5);
  g[3].emplace_back(0, 1);
  g[0].emplace_back(2, 3);
  g[3].emplace_back(2, 2);
  g[1].emplace_back(4, 5);
  g[2].emplace_back(1, 1);
  ASSERT_THAT(graph::spfa(g, 3), testing::ElementsAre(1, 3, 2, 0, 8));
  ASSERT_THAT(graph::dijkstra(g, 3), testing::ElementsAre(1, 3, 2, 0, 8));
}
