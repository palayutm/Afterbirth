#ifndef SHORTEST_PATH_H
#define SHORTEST_PATH_H

#include <algorithm>
#include <queue>
#include <vector>

namespace graph {
const long long INF = 1e18;
typedef std::vector<std::vector<std::pair<int, int>>> Graph;
std::vector<long long> spfa(const Graph &g, int src) {
  int n = g.size();
  std::vector<long long> distance(n, INF);
  std::vector<bool> in_queue(n);
  std::queue<int> q;
  q.push(src);
  distance[src] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    in_queue[u] = false;
    for (auto e : g[u]) {
      int v = e.first, w = e.second;
      if (distance[u] + w < distance[v]) {
        distance[v] = distance[u] + w;
        if (!in_queue[v]) {
          in_queue[v] = true;
          q.push(v);
        }
      }
    }
  }
  return distance;
}

using std::greater;
using std::pair;
using std::vector;
vector<long long> dijkstra(const Graph &g, int src) {
  int n = g.size();
  vector<long long> distance(n, INF);
  std::priority_queue<pair<long long, int>, vector<pair<long long, int>>, greater<pair<long long, int>>> q;
  vector<bool> done(n);
  q.push({0, src});
  distance[src] = 0;
  while (!q.empty()) {
    int u = q.top().second;
    q.pop();
    if (done[u]) {
      continue;
    }
    done[u] = true;
    for (auto e : g[u]) {
      int v = e.first, w = e.second;
      if (!done[v] && distance[u] + w < distance[v]) {
        distance[v] = distance[u] + w;
        q.push({distance[v], v});
      }
    }
  }
  return distance;
}
} // namespace graph

#endif /* SHORTEST_PATH_H */
