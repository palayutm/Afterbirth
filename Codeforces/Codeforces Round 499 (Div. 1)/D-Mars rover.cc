#include <bits/stdc++.h>

using namespace std;

const int N = 1000005;

int ans[N];
vector<int> g[N];
int a[N];
string type[N];

int dfs1(int u) {
  if (type[u] == "NOT") {
    a[u] = 1 - dfs1(g[u][0]);
  } else if (type[u] == "AND") {
    a[u] = (dfs1(g[u][0]) & dfs1(g[u][1]));
  } else if (type[u] == "OR") {
    a[u] = (dfs1(g[u][0]) | dfs1(g[u][1]));
  } else if (type[u] == "XOR"){
    a[u] = (dfs1(g[u][0]) ^ dfs1(g[u][1]));
  }
  return a[u];
}

void dfs2(int u, int val) {
  if (type[u] == "IN") {
    if (val == 2 || val != a[u]) ans[u] = 1;
  } else{
    if (val == 2) {
      for (auto v : g[u]) {
        dfs2(v, val);
      }
      return;
    }
    if (type[u] == "NOT") {
      dfs2(g[u][0], 1 - val);
    } else{
      auto cal = [](int x, int y, string op) {
        if (op == "OR") return x | y;
        if (op == "XOR") return x ^ y;
        return x & y;
      };
      for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
          if (cal(i, j, type[u]) == val) {
            if (a[g[u][0]] == i) dfs2(g[u][1], j);
            if (a[g[u][1]] == j) dfs2(g[u][0], i);
          }
        }
      }
    }
  }
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> type[i];
    if (type[i] == "IN") {
      cin >> a[i];
    } else if (type[i] == "NOT") {
      int v;
      cin >> v;
      g[i].push_back(v);
    } else {
      int v1, v2;
      cin >> v1 >> v2;
      g[i].push_back(v1);
      g[i].push_back(v2);
    }
  }
  dfs1(1);
  dfs2(1, 1);
  for (int i = 1; i <= n; i++) {
    if (type[i] == "IN") {
      cout << ans[i];
    }
  }
  return 0;
}
