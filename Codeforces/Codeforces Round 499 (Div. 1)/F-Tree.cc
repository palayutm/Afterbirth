#include <bits/stdc++.h>

using namespace std;

const int mod = 998244353;

int inverse(int a, int md) {
  return a == 1 ? a : (long long)(md - md / a) * inverse(md % a, md) % md;
}

namespace ntt {
int fast_pow(int x, int y, int z) {
  long long ret = 1;
  for (; y; y >>= 1, x = (long long)x * x % z) {
    if (y & 1)
      ret = ret * x % z;
  }
  return ret;
}

int revv(int x, int bits) {
  int ret = 0;
  for (int i = 0; i < bits; i++) {
    ret <<= 1, ret |= x & 1, x >>= 1;
  }
  return ret;
}

void ntt(vector<int> &a, bool rev, int mod, int root) {
  int n = a.size(), bits = 32 - __builtin_clz(n) - 1;
  for (int i = 0; i < n; i++) {
    int j = revv(i, bits);
    if (i < j) swap(a[i], a[j]);
  }
  for (int k = 1; k < n; k <<= 1) {
    int e = fast_pow(root, (mod - 1) / 2 / k, mod);
    if (rev) e = fast_pow(e, mod - 2, mod);
    for (int i = 0; i < n; i += 2 * k) {
      long long w = 1;
      for (int j = 0; j < k; j++, w = w * e % mod) {
        int x = a[i + j], y = w * a[i + j + k] % mod;
        a[i + j] = (x + y) % mod, a[i + j + k] = (x - y + mod) % mod;
      }
    }
  }
  if (rev) {
    int inv = fast_pow(n, mod - 2, mod);
    for (int i = 0; i < n; i++) a[i] = (long long)a[i] * inv % mod;
  }
}

//const int mod = (119 << 23) + 1, root = 3; // = 998244353
// For p < 2^30 there is also e.g. (5 << 25, 3), (7 << 26, 3),
// (479 << 21, 3) and (483 << 21, 5). The last two are > 10^9.

vector<int> convolution(const vector<int> &a, const vector<int> &b, int mod = (119 << 23) + 1, int root = 3) {
  int sz = (int)a.size() + (int)b.size() - 1;
  int L = sz > 1 ? 32 - __builtin_clz(sz - 1) : 0, n = 1 << L;
  vector<int> av(n), bv(n);
  copy(a.begin(), a.end(), av.begin());
  copy(b.begin(), b.end(), bv.begin());
  ntt(av, false, mod, root), ntt(bv, false, mod, root);
  for (int i = 0; i < n; i++) {
    av[i] = (long long)av[i] * bv[i] % mod;
  }
  ntt(av, true, mod, root);
  return av;
}
} // namespace ntt

vector<int> operator * (const vector<int>& a, const vector<int>& b) {
  auto ret = ntt::convolution(a, b);
  ret.resize(a.size() + b.size() - 1);
  return ret;
}

vector<int> operator + (vector<int> a, vector<int> b) {
  if (a.size() > b.size()) swap(a, b);
  for (int i = 0; i < a.size(); i++) {
    (b[i] += a[i]) %= mod;
  }
  return b;
}

vector<int> operator - (vector<int> a, vector<int> b) {
  if (a.size() > b.size()) swap(a, b);
  for (int i = 0; i < a.size(); i++) {
    b[i] = (b[i] - a[i] + mod) % mod;
  }
  return b;
}

const int N = 100005;

vector<int> g[N];
int fa[N], son[N], sz[N];

void dfs1(int u, int f) {
  fa[u] = f;
  sz[u] = 1;
  for (auto v : g[u]) {
    if (v != f) {
      dfs1(v, u);
      if (sz[v] > sz[son[u]]) son[u] = v;
      sz[u] += sz[v];
    }
  }
}

vector<int> ans[N];

pair<vector<int>, vector<int>> dc(vector<pair<vector<int>, vector<int>>> polys) {
  if (polys.size() == 1) return polys[0];
  int m = polys.size() / 2;
  auto l = dc({polys.begin(), polys.begin() + m});
  auto r = dc({polys.begin() + m, polys.end()});
  return {l.first * r.first, l.second + l.first * r.second};
}

void dfs2(int u) {
  vector<pair<vector<int>, vector<int>>> polys;
  for (int p = u; p; p = son[p]) {
    vector<int> poly = {0, 1};
    for (auto v : g[p]) {
      if (v != fa[p] && v != son[p]) dfs2(v), poly = poly + poly * ans[v];
    }
    polys.emplace_back(poly, poly);
  }
  ans[u] = dc(polys).second;
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  long long X;
  cin >> n >> X;
  for (int i = 0; i < n - 1; i++) {
    int u, v;
    cin >> u >> v;
    g[v].push_back(u);
    g[u].push_back(v);
  }
  dfs1(1, 0);
  dfs2(1);
  /*  for (auto x : ans[1]) {
    cout << x << " ";
  }
  cout << endl;*/
  long long as = 0, x = 1, y = 1;
  for (int i = 1; i < ans[1].size(); i++) {
    if (i > 1) {
      x = x * (X % mod + i - 1) % mod;
      y = y * (i - 1) % mod;
    }
    long long v = x * inverse(y, mod) % mod;
    (as += v * ans[1][i]) %= mod;
  }
  cout << as << endl;
  return 0;
}
