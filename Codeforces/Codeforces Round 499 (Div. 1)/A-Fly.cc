#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, m;
  cin >> n >> m;
  vector<int> a(n + 1), b(n + 1);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  for (int i = 0; i < n; i++) {
    cin >> b[i];
  }
  a[n] = a[0];
  b[n] = b[0];
  double low = 0, high = 2e9;
  for (int i = 0; i < 300; i++) {
    double mid = (low + high) / 2;
    auto cal = [&](double fuel, double x) {
      return (fuel + m) / x;
    };
    bool ok = true;
    double now = mid;
    for (int i = 0; i <= n; i++) {
      if (i >= 1) {
        if (now < cal(now, b[i])) {
          ok = false;
          break;
        }
        now -= cal(now, b[i]);
      }
      if (i < n) {
        if (now < cal(now, a[i])) {
          ok = false;
          break;
        }
        now -= cal(now, a[i]);
      }
    }
    if (ok) high = mid;
    else low = mid;
  }
  if (high > 1e9 + 5) {
    puts("-1");
  } else {
    printf("%.10f\n", high);    
  }
  return 0;
}
