#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

template <class T> struct FenwickTree {
  int N;
  std::vector<T> in;
  FenwickTree(int N) : N(N), in(N) {}
  void add(int at, T by) {
    if (!by) return;
    for (int i = at; i < N; i += (i & -i)) {
      in[i] += by;
    }
  }
  T query(int at) {
    T sum = 0;
    for (int i = at; i; i -= (i & -i)) {
      sum += in[i];
    }
    return sum;
  }
};

FenwickTree<int> ft(100001);
int ans[100001];

struct A {
  int x, y, z, type, id, mul;
  bool operator<(const A& o) const {
    if (x != o.x) return x < o.x;
    if (y != o.y) return y < o.y;
    if (z != o.z) return z < o.z;
    if (type != o.type) return type < o.type;
    return id < o.id;
  }
};

vector<A> query;

vector<A> cdq(vector<A> vec) {
  if (vec.size() <= 1)
    return vec;
  int m = vec.size() / 2;
  auto left = cdq({vec.begin(), vec.begin() + m});
  auto right = cdq({vec.begin() + m, vec.end()});
  vector<A> ret;
  int h1 = 0, h2 = 0;
  while (h1 < left.size() || h2 < right.size()) {
    if (h2 == right.size() || (h1 < left.size() && left[h1].y <= right[h2].y)) {
      ft.add(left[h1].z, 1 - left[h1].type);
      ret.push_back(left[h1++]);
    } else {
      if (right[h2].type == 1) {
        ans[right[h2].id] += ft.query(right[h2].z) * right[h2].mul;
      }
      ret.push_back(right[h2++]);
    }
  }
  for (auto q : left) {
    ft.add(q.z, -(1 - q.type));
  }
  return ret;
}

void solve() {
  sort(query.begin(), query.end());
  cdq(query);
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int ign, n, m, K;
  cin >> ign >> ign >> ign >> n >> m >> K;
  vector<int> mx(3), mn(3, inf);
  for (int i = 0; i < n; i++) {
    vector<int> vec(3);
    cin >> vec[0] >> vec[1] >> vec[2];
    for (int j = 0; j < 3; j++) {
      mx[j] = max(mx[j], vec[j]);
      mn[j] = min(mn[j], vec[j] - 1);
    }
  }
  for (int i = 0; i < m; i++) {
    int x, y, z;
    cin >> x >> y >> z;
    query.push_back(A{x, y, z, 0, -1, 0});
  }
  vector<vector<int>> Q1(K + 1), Q2(K + 1);
  for (int i = 0; i < K; i++) {
    vector<int> vec(3);
    cin >> vec[0] >> vec[1] >> vec[2];
    auto& v1 = Q1[i];
    auto& v2 = Q2[i];
    v1 = mx, v2 = mn;
    for (int j = 0; j < 3; j++) {
      v1[j] = max(v1[j], vec[j]);
      v2[j] = min(v2[j], vec[j] - 1);
    }
  }
  Q1.back() = mx;
  Q2.back() = mn;
  for (int i = 0; i < Q1.size(); i++) {
    auto& v1 = Q1[i];
    auto& v2 = Q2[i];
    for (int j = 0; j < 8; j++) {
      vector<int> vec;
      int cnt = 0;
      for (int k = 0; k <= 2; k++) {
        if ((1 << k) & j) vec.push_back(v2[k]), cnt++;
        else vec.push_back(v1[k]);
      }
      query.push_back(A{vec[0], vec[1], vec[2], 1, i, (cnt & 1) ? -1 : 1});
    }
  }
  solve();
  if (ans[K]) {
    puts("INCORRECT");
  } else {
    puts("CORRECT");
    for (int i = 0; i < K; i++) {
      auto& v1 = Q1[i];
      auto& v2 = Q2[i];
      if (v1 == mx && v2 == mn) {
        puts("OPEN");
      } else if (ans[i]) {
        puts("CLOSED");
      } else {
        puts("UNKNOWN");
      }
    }
  }
  return 0;
}
