#include <bits/stdc++.h>

using namespace std;

int query(int x) {
  cout << x << endl;
  int v;
  cin >> v;
  if (v == 0) exit(0);
  return v;
}

int main(int argc, char *argv[]) {
  int m, n;
  cin >> m >> n;
  vector<int> a(n);
  for (int i = 0; i < n; i++) {
     a[i] = query(1);
  }
  int low = 1, high = m, now = 0;
  while (low <= high) {
    int mid = (low + high) / 2;
    if (query(mid) * a[now] == 1) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
    now = (now + 1) % n;
  }
  return 0;
}
