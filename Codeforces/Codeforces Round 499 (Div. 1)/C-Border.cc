#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, K;
  cin >> n >> K;
  int d = K;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    d = __gcd(d, a[i]);
  }
  cout << K / d << endl;
  for (int i = 0; i < K; i += d) {
    cout << i << " ";
  }
  return 0;
}
