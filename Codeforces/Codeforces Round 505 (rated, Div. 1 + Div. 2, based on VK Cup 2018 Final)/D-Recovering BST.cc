#include <bits/stdc++.h>

using namespace std;

int f[701][701][2];

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n, 1);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  vector<vector<int>> g(n, vector<int>(n));
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      g[i][j] = (__gcd(a[i], a[j]) > 1);
    }
  }
  for (int i = 0; i <= n; i++) {
    f[i][i][0] = f[i][i][1] = 1;
  }
  for (int d = 1; d < n; d++) {
    for (int l = 0; l + d <= n; l++) {
      int r = l + d;
      for (int k = l; k < r; k++) {
        if (l > 0) f[l][r][0] |= (f[l][k][1] && f[k + 1][r][0] && g[l - 1][k]);
        if (r < n) f[l][r][1] |= (f[l][k][1] && f[k + 1][r][0] && g[k][r]);
      }
    }
  }
  for (int i = 0; i < n; i++) {
    if (f[0][i][1] && f[i + 1][n][0]) {
      cout << "Yes" << endl;
      return 0;
    }
  }
  cout << "No" << endl;
  return 0;
}
