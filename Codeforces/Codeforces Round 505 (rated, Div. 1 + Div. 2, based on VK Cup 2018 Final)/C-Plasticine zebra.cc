#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  string s;
  cin >> s;
  int n = s.size();
  s += s;
  int ans = 1;
  for (int i = 1, now = 1; i < s.size(); i++) {
    if (s[i] != s[i - 1]) {
      now++;
    } else {
      now = 1;
    }
    ans = max(ans, now);
  }
  ans = min(ans, n);
  cout << ans << endl;
  return 0;
}
