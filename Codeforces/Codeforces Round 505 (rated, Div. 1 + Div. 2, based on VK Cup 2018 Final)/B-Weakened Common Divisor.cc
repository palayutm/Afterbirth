#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  long long d = 0, x, y;
  for (int i = 0; i < n; i++) {
    cin >> x >> y;
    if (i == 0) d = x * y;
    else d = __gcd(d, x * y);
  }
  if (d > 1) {
    auto solve = [&](long long x) {
      for (int i = 2; i * i <= x; i++) {
        if (x % i == 0 && d % i == 0) {
          cout << i << endl;
          exit(0);
        }
        while (x % i == 0) x /= i;
      }
      if (x > 1 && d % x == 0) {
        cout << x << endl;
        exit(0);
      }
    };
    solve(x);
    solve(y);
  } else {
    cout << -1 << endl;
  }
  return 0;
}
