#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  string s;
  int n;
  cin >> n >> s;
  cout << (n > 1 && set<char>(s.begin(), s.end()).size() == s.size() ? "No" : "Yes") << endl;
  return 0;
}
