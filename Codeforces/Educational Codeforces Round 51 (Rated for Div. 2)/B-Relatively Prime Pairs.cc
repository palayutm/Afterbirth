#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  long long L, R;
  cin >> L >> R;
  cout << "YES" << endl;
  for (long long i = L; i < R; i += 2) {
    cout << i << " " << i + 1 << endl;
  }
  return 0;
}
