#include <bits/stdc++.h>

using namespace std;

const long long INF = 1e18;

typedef std::vector<std::vector<std::pair<int, int>>> Graph;
vector<long long> dijkstra(const Graph &g, int src) {
  int n = g.size();
  vector<long long> distance(n, INF);
  std::priority_queue<pair<long long, int>, vector<pair<long long, int>>, greater<pair<long long, int>>> q;
  vector<bool> done(n);
  q.push({0, src});
  distance[src] = 0;
  while (!q.empty()) {
    int u = q.top().second;
    q.pop();
    if (done[u]) {
      continue;
    }
    done[u] = true;
    for (auto e : g[u]) {
      int v = e.first, w = e.second;
      if (!done[v] && distance[u] + w < distance[v]) {
        distance[v] = distance[u] + w;
        q.push({distance[v], v});
      }
    }
  }
  return distance;
}

template <class T> struct SparseTable {
  std::vector<std::vector<T>> f;
  std::vector<int> lg;
  SparseTable() {}
  SparseTable(const std::vector<T> &a) { init(a); }
  void init(const std::vector<T> &a) {
    int n = a.size();
    lg.assign(n + 1, 0);
    for (int i = 1, j = 0; i <= n; i++) {
      if (i == (1 << (j + 1))) j++;
      lg[i] = j;
    }
    f.assign(lg.back() + 1, std::vector<T>(a.size()));
    f[0] = a;
    for (int j = 1; (1 << j) <= n; j++) {
      for (int i = 0; i + (1 << j) - 1 < n; i++) {
        f[j][i] = min(f[j - 1][i], f[j - 1][i + (1 << (j - 1))]);
      }
    }
  }
  T query(int l, int r) {
    int j = lg[r - l + 1];
    return min(f[j][l], f[j][r - (1 << j) + 1]);
  }
};

struct LCA {
  const std::vector<std::vector<int>>& g;
  SparseTable<std::pair<int, int>> st;
  std::vector<int> sp;
  std::vector<int> dep, ST, ED;

  LCA(const std::vector<std::vector<int>>& graph, int root = 1) : g(graph) {
    int sz = graph.size();
    dep = ST = ED = std::vector<int>(sz);
    dfs(root, -1, 0);
    std::vector<std::pair<int, int>> vec;
    for (int i = 0; i < sp.size(); i++) {
      vec.emplace_back(ST[sp[i]], sp[i]);
    }
    st.init(vec);
  }
  
  void dfs(int u, int fa, int d) {
    dep[u] = d + 1;
    ST[u] = sp.size();
    sp.push_back(u);
    for (auto v : g[u]) {
      if (v != fa) {
        dfs(v, u, d + 1);
        sp.push_back(u);
      }
    }
    ED[u] = (int)sp.size() - 1;
  }

  int query(int u, int v) {
    int l = ST[u], r = ST[v];
    if (l > r) std::swap(l, r);
    return st.query(l, r).second;
  }
};

set<pair<int, int>> se;
bool vis[123456];
long long dis[123456];
vector<vector<pair<int, int>>> g;

void dfs(int u, int fa) {
  vis[u] = true;
  for (auto [v, w] : g[u]) {
    if (v != fa) {
      if (!vis[v]) {
        dis[v] = dis[u] + w;
        dfs(v, u);
      } else {
        se.insert({u, v});
      }
    }
  }
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  g.resize(n + 1);
  for (int i = 0; i < m; i++) {
    int u, v, w;
    cin >> u >> v >> w;
    g[u].emplace_back(v, w);
    g[v].emplace_back(u, w);
  }
  dfs(1, 0);
  vector<tuple<int, int, int>> edges;
  vector<vector<int>> sg(n + 1);
  for (int i = 1; i <= n; i++) {
    for (auto [v, w] : g[i]) {
      if (!se.count({i, v})) {
        sg[i].push_back(v);
      } else {
        edges.emplace_back(i, v, w);
      }
    }
  }
  LCA lca(sg);
  map<int, vector<long long>> mp;
  for (auto [u, v, w] : edges) {
    if (!mp.count(u)) {
      mp[u] = dijkstra(g, u);
    }
  }
  int Q;
  cin >> Q;
  while (Q--) {
    int u, v;
    cin >> u >> v;
    long long ans = dis[u] + dis[v] - 2 * dis[lca.query(u, v)];
    for (auto [x, y, w] : edges) {
      ans = min(ans, mp[x][u] + mp[y][v] + w);
    }
    cout << ans << endl;
  }
  return 0;
}
