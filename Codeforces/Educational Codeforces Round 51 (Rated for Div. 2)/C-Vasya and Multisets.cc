#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n, 0), b(111);
  map<int, int> mp;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    mp[a[i]]++;
  }
  int cnt = 0, p = -1;
  for (auto [k, v] : mp) {
    if (v == 1) {
      b[k] = cnt % 2;
      cnt++;
    } else if (v > 2) {
      p = k;
    }
  }
  if (cnt % 2 == 1 && p == -1) {
    cout << "NO" << endl;
  } else {
    cout << "YES" << endl;
    for (auto x : a) {
      if (cnt % 2 == 1 && p == x) {
        cout << "B";
        p = -1;
      } else cout << (b[x] ? "B" : "A");
    }
    cout << endl;
  }
  return 0;
}
