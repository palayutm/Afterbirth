#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int T;
  cin >> T;
  while (T--) {
    bool o1 = false, o2 = false, o3 = false;
    string s;
    cin >> s;
    vector<int> vec;
    for (int i = 0; i < s.size(); i++) {
      if (s[i] >= 'a' && s[i] <= 'z') {
        if (o1) vec.push_back(i);
        o1 = true;
      } else if (s[i] >= 'A' && s[i] <= 'Z') {
        if (o2) vec.push_back(i);
        o2 = true;
      } else {
        if (o3) vec.push_back(i);
        o3 = true;
      }
    }
    if (!o1) s[vec.back()] = 'a', vec.pop_back();
    if (!o2) s[vec.back()] = 'A', vec.pop_back();
    if (!o3) s[vec.back()] = '0', vec.pop_back();
    cout << s << endl;
  }
  return 0;
}
