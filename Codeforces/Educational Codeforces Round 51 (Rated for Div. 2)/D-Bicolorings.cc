#include <bits/stdc++.h>

using namespace std;

const int mod = 998244353;

long long f[1111][2222][4];

void add(long long &x, long long y) {
  x = (x + y) % mod;
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, K;
  cin >> n >> K;
  f[1][1][0] = f[1][1][3] = f[1][2][1] = f[1][2][2] = 1;
  for (int i = 1; i < n; i++) {
    for (int j = 1; j <= 2 * i; j++) {
      add(f[i + 1][j][0], f[i][j][0]);
      for (int k = 1; k < 4; k++) {
        add(f[i + 1][j + 1][k], f[i][j][0]);
      }
      add(f[i + 1][j][3], f[i][j][3]);
      for (int k = 0; k < 3; k++) {
        add(f[i + 1][j + 1][k], f[i][j][3]);
      }
      add(f[i + 1][j + 2][2], f[i][j][1]);
      add(f[i + 1][j][1], f[i][j][1]);
      add(f[i + 1][j][0], f[i][j][1]);
      add(f[i + 1][j][3], f[i][j][1]);
      add(f[i + 1][j][2], f[i][j][2]);
      add(f[i + 1][j + 2][1], f[i][j][2]);
      add(f[i + 1][j][0], f[i][j][2]);
      add(f[i + 1][j][3], f[i][j][2]);
    }
  }
  long long ans = 0;
  for (int i = 0; i < 4; i++) {
    add(ans, f[n][K][i]);
  }
  cout << ans << endl;
  return 0;
}
