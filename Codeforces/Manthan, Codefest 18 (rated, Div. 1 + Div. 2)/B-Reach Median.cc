#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, S;
  cin >> n >> S;
  vector<int> a(n);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  long long ans = 0;
  sort(a.begin(), a.end());
  for (int i = 0; i < n; i++) {
    if (i < n / 2 && a[i] >= S) ans += a[i] - S;
    if (i > n / 2 && a[i] <= S) ans += S - a[i];
    if (i == n / 2) ans += abs(S - a[i]);
  }
  cout << ans << endl;
  return 0;
}
