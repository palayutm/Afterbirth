#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;
 
template <class T> struct SparseTable {
  std::vector<std::vector<T>> f;
  std::vector<int> lg;
  SparseTable() {}
  SparseTable(const std::vector<T> &a) { init(a); }
  void init(const std::vector<T> &a) {
    int n = a.size();
    lg.assign(n + 1, 0);
    for (int i = 1, j = 0; i <= n; i++) {
      if (i == (1 << (j + 1))) j++;
      lg[i] = j;
    }
    f.assign(lg.back() + 1, std::vector<T>(a.size()));
    f[0] = a;
    for (int j = 1; (1 << j) <= n; j++) {
      for (int i = 0; i + (1 << j) - 1 < n; i++) {
        f[j][i] = max(f[j - 1][i], f[j - 1][i + (1 << (j - 1))]);
      }
    }
  }
  T query(int l, int r) {
    int j = lg[r - l + 1];
    return max(f[j][l], f[j][r - (1 << j) + 1]);
  }
};

int main(int argc, char *argv[]) {
  int n, K;
  scanf("%d%d", &n, &K);
  vector<int> a(n);
  for (int i = 0; i < n; i++) {
    scanf("%d", &a[i]);
  }
  SparseTable<int> st(a);
  vector<vector<int>> b(K - 1);
  for (int i = 0; i < n; i++) {
    b[i % (K - 1)].push_back(i);
  }
  long long ans = 0;
  for (int z = 0; z < K - 1; z++) {
    auto& vec = b[z];
    for (int j = 0; j < vec.size(); j++) {
      if (j > 0) {
        int vv = st.query(vec[j - 1] + 1, vec[j]);
        int tl = 0;
        {
          int low = 0, high = j - 1;
          while (low <= high) {
            int mid = (low + high) / 2;
            if (st.query(vec[mid], vec[j - 1]) < vv) high = mid - 1;
            else low = mid + 1;
          }
          tl = low;
        }
        int tr = 0;
        {
          int low = j + 1, high = (int)vec.size() - 1;
          while (low <= high) {
            int mid = (low + high) / 2;
            if (st.query(vec[j - 1] + 1, vec[mid]) == vv) low = mid + 1;
            else high = mid - 1;
          }
          tr = high;
        }
        ans = (ans + 1LL * (j - tl) * (tr - j + 1) % mod * vv) % mod;
      }
      {
        int low = j + 1, high = (int)vec.size() - 1;
        while (low <= high) {
          int mid = (low + high) / 2;
          if (st.query(vec[j], vec[mid]) == a[vec[j]]) low = mid + 1;
          else high = mid - 1;
        }
        //        cout << a[vec[j]] << " " << high << endl;
        ans = (ans + (high - j) * 1LL * a[vec[j]]) % mod;
      }
    }
  }
  cout << ans << endl;
  return 0;
}
