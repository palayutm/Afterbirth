#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<set<int>> g(n);
  for (int i = 0; i < n - 1; i++) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    g[u].insert(v);
    g[v].insert(u);
  }
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    a[i]--;
  }
  if (a[0] != 0) {
    cout << "No" << endl;
    return 0;
  }
  reverse(a.begin(), a.end());
  auto check = [&]() {
    queue<int> q;
    vector<bool> vis(n);
    q.push(a.back());
    a.pop_back();
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      while (!g[u].empty()) {
        int v = a.back();
        a.pop_back();
        if (!g[u].count(v)) return false;
        g[u].erase(v);
        g[v].erase(u);
        q.push(v);
      }
    }
    return a.empty();
  };
  cout << (check() ? "Yes" : "No") << endl;
  return 0;
}
