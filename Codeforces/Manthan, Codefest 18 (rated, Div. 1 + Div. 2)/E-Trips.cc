#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m, K;
  cin >> n >> m >> K;
  vector<set<int>> g(n);
  vector<pair<int, int>> vec;
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    vec.emplace_back(u, v);
    g[u].insert(v);
    g[v].insert(u);
  }
  priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
  for (int i = 0; i < n; i++) {
    q.push({g[i].size(), i});
  }
  int now = n;
  vector<int> ans;
  vector<bool> vis(n);
  for (int i = m - 1; i >= 0; i--) {
    while (!q.empty()) {
      int val, u;
      tie(val, u) = q.top();
      if (val < K) {
        q.pop();
        if (g[u].size() == val) {
          vis[u] = true;
          now--;
          for (auto v : g[u]) {
            g[v].erase(u);
            q.push({g[v].size(), v});
          }
          g[u].clear();
        }
      } else break;
    }
    auto [x, y] = vec[i];
    if (!vis[x] && !vis[y]) {
      g[x].erase(y);
      g[y].erase(x);
      q.push({g[x].size(), x});
      q.push({g[y].size(), y});
    }
    ans.push_back(now);
  }
  reverse(ans.begin(), ans.end());
  for (auto x : ans) {
    cout << x << endl;
  }
  return 0;
}
