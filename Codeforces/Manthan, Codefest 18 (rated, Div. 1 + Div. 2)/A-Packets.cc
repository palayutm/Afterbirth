#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n;
  cin >> n;
  int ans = 0;
  for (int i = 0; i < 31; i++) {
    if ((1 << i) & n) {
      ans = i + 1;
    }
  }
  cout << ans << endl;
  return 0;
}
