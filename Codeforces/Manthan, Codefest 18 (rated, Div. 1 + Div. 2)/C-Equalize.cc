#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n;
  string s, t;
  cin >> n >> s >> t;
  vector<int> a(s.size()), b(s.size());
  for (int i = 0; i < n; i++) {
    a[i] = s[i] - '0';
    b[i] = t[i] - '0';
  }
  long long ans = 0;
  for (int i = 0; i < n; i++) {
    if (i + 1 < n && a[i] != a[i + 1] && a[i] != b[i] && a[i + 1] != b[i + 1]) {
      ans++;
      a[i] ^= 1;
      a[i + 1] ^= 1;
    }
    if (a[i] != b[i]) {
      ans++;
      a[i] ^= 1;
    }
  }
  cout << ans << endl;
  return 0;
}
