#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  string s;
  cin >> s;
  vector<vector<int>> f(s.size() + 1, vector<int>(3, -inf));
  f[0][0] = 0;
  for (int i = 0; i < s.size(); i++) {
    f[i][0] = max(f[i][1], max(f[i][0], f[i][2]));
    for (int j = 0; j < 3; j++) {
      int v = (j + s[i] - '0') % 3;
      f[i + 1][v] = max(f[i + 1][v], f[i][j] + (v == 0));
    }
  }
  cout << max(f[s.size()][0], max(f[s.size()][1], f[s.size()][2])) << endl;
  return 0;
}
