#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  vector<int> b;
  for (int i = 1; i < n; i++) {
    if (a[i] == 1) {
      b.push_back(a[i - 1]);
    }
  }
  b.push_back(a.back());
  cout << b.size() << endl;
  for (auto x : b) {
    cout << x << " ";
  }
  return 0;
}
