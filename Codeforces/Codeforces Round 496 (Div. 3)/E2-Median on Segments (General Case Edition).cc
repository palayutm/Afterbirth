#include <bits/stdc++.h>

using namespace std;

template <class T> struct FenwickTree {
  std::vector<T> in;
  int N;
  FenwickTree(int N) : N(N), in(N) {}
  void add(int at, T by) {
    for (int i = at; i < N; i += (i & -i)) {
      in[i] += by;
    }
  }
  T query(int at) {
    T sum = 0;
    for (int i = at; i; i -= (i & -i)) {
      sum += in[i];
    }
    return sum;
  }
};

FenwickTree<int> ft(400005);

struct Query {
  int x, y;
  int id;
};

int ans[200005];

vector<Query> cdq(vector<Query> vec) {
  if (vec.size() <= 1)
    return vec;
  int m = vec.size() / 2;
  auto left = cdq({vec.begin(), vec.begin() + m});
  auto right = cdq({vec.begin() + m, vec.end()});
  vector<Query> ret;
  int h1 = 0, h2 = 0;
  while (h1 < left.size() || h2 < right.size()) {
    if (h2 == right.size() || (h1 < left.size() && left[h1].x < right[h2].x)) {
      ft.add(left[h1].y, 1);
      ret.push_back(left[h1++]);
    } else {
      ans[right[h2].id] += ft.query(right[h2].y);
      ret.push_back(right[h2++]);
    }
  }
  for (auto q : left) {
    ft.add(q.y, -1);
  }
  return ret;
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  vector<int> a(n + 1), b(n + 1), c(n + 1);
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
    b[i] = b[i - 1] + (a[i] <= m);
    c[i] = c[i - 1] + (a[i] < m);
  }
  vector<Query> vec;
  for (int i = 0; i <= n; i++) {
    vec.push_back({-(c[i] * 2 - i), b[i] * 2 - i + 200000, i});
  }
  cdq(vec);
  long long sum = 0;
  for (int i = 0; i <= n; i++) {
    sum += ans[i];
  }
  cout << sum << endl;
  return 0;
}
