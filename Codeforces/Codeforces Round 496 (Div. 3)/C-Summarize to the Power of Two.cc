#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n, 0);
  map<int, int> mp;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    mp[a[i]]++;
  }
  int ans = 0;
  for (int i = 0; i < n; i++) {
    mp[a[i]]--;
    bool ok = false;
    for (int j = 0; j <= 30; j++) {
      if (mp[(1 << j) - a[i]] > 0) {
        ok = true;
      }
    }
    if (!ok) {
      ans++;
    }
    mp[a[i]]++;
  }
  cout << ans << endl;
  return 0;
}
