#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  string s, t;
  cin >> s >> t;
  reverse(s.begin(), s.end());
  reverse(t.begin(), t.end());
  int n = s.size() + t.size();
  for (int i = 0; i < s.size() && i < t.size(); i++) {
    if (s[i] == t[i]) {
      n -= 2;
    } else break;
  }
  cout << n << endl;
  return 0;
}
