#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int K;
vector<string> ans;
vector<vector<int>> vec;

void dfs(int p, string& s) {
  if (ans.size() >= K) return;
  if (p == vec.size()) {
    ans.push_back(s);
    return;
  }
  for (auto x : vec[p]) {
    s[x] = '1';
    dfs(p + 1, s);
    s[x] = '0';
  }
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  cin >> n >> m >> K;
  vector<vector<int>> g(n + 1);
  map<pair<int, int>, int> mp;
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    mp[{u, v}] = mp[{v, u}] = i;
    g[u].push_back(v);
    g[v].push_back(u);
  }
  queue<int> q;
  q.push(1);
  vector<int> dis(n + 1, inf);
  dis[1] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (auto v : g[u]) {
      if (dis[v] == inf) {
        dis[v] = dis[u] + 1;
        q.push(v);
      }
    }
  }

  for (int i = 2; i <= n; i++) {
    vector<int> tt;
    for (auto v : g[i]) {
      if (dis[i] - 1 == dis[v]) {
        tt.push_back(mp[{i, v}]);
      }
    }
    vec.push_back(tt);
  }
  string s(m, '0');
  dfs(0, s);
  cout << ans.size() << endl;
  for (auto x : ans) {
    cout << x << endl;
  }
  return 0;
}
