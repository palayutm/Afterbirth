#include <bits/stdc++.h>

using namespace std;

const int mod = 998244353;

long long inverse(int a, int md = mod) {
  return a == 1 ? a : (long long)(md - md / a) * inverse(md % a, md) % md;
}

void add(long long& x, long long y) {
  x = (x + y) % mod;
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  map<int, vector<pair<int, int>>> mp;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      int v;
      cin >> v;
      mp[-v].emplace_back(i, j);
    }
  }
  vector<vector<pair<int, int>>> a;
  for (auto e : mp) {
    a.push_back(e.second);
  }
  vector<long long> pa(a.size() + 1);
  int r, c;
  cin >> r >> c;
  r--, c--;
  int st = 0;
  auto pb = pa, pc = pa, pd = pa, pn = pa;
  for (int i = a.size() - 1; i >= 0; i--) {
    pn[i] = pn[i + 1] + a[i].size();
    for (auto [x, y] : a[i]) {
      add(pa[i], x);
      add(pb[i], x * x);
      add(pc[i], y);
      add(pd[i], y * y);
      if (x == r && y == c) {
        st = i;
      }
    }
    add(pa[i], pa[i + 1]);
    add(pb[i], pb[i + 1]);
    add(pc[i], pc[i + 1]);
    add(pd[i], pd[i + 1]);
  }
  long long ans = 0;
  if (st + 1 < a.size()) {
    ans = r * r + c * c;
    add(ans, pb[st + 1] * inverse(pn[st + 1]) % mod);
    add(ans, pd[st + 1] * inverse(pn[st + 1]) % mod);
    add(ans, -pa[st + 1] * inverse(pn[st + 1]) % mod * 2 * r % mod + mod);
    add(ans, -pc[st + 1] * inverse(pn[st + 1]) % mod * 2 * c % mod + mod);
    long long now = inverse(pn[st + 1]);
    for (int i = st + 1; i + 1 < a.size(); i++) {
      for (auto [x, y] : a[i]) {
        add(ans, (x * x + y * y) * now % mod);
        add(ans, pb[i + 1] * inverse(pn[i + 1]) % mod * now % mod);
        add(ans, pd[i + 1] * inverse(pn[i + 1]) % mod * now % mod);
        add(ans, -pa[i + 1] * inverse(pn[i + 1]) % mod * 2 * x % mod * now % mod + mod);
        add(ans, -pc[i + 1] * inverse(pn[i + 1]) % mod * 2 * y % mod * now % mod + mod);
      }
      add(now, now * a[i].size() % mod * inverse(pn[i + 1]) % mod);
    }
  }
  cout << ans << endl;
  return 0;
}
