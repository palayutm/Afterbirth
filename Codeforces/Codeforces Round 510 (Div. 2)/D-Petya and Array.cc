#include <bits/stdc++.h>

using namespace std;

template <class T> struct FenwickTree {
  int N;
  std::vector<T> in;
  FenwickTree(int N) : N(N), in(N) {}
  void add(int at, T by) {
    for (int i = at; i < N; i += (i & -i)) {
      in[i] += by;
    }
  }
  T query(int at) {
    T sum = 0;
    for (int i = at; i; i -= (i & -i)) {
      sum += in[i];
    }
    return sum;
  }
};

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  long long T;
  cin >> n >> T;
  vector<long long> a(n + 1, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i + 1];
    a[i + 1] += a[i];
  }
  vector<long long> vec;
  for (auto x : a) {
    vec.push_back(x);
    vec.push_back(x - T);
  }
  sort(vec.begin(), vec.end());
  vec.erase(unique(vec.begin(), vec.end()), vec.end());
  auto index = [&](long long v) {
    return lower_bound(vec.begin(), vec.end(), v) - vec.begin() + 1;
  };
  FenwickTree<int> fw(vec.size() + 1);
  long long ans = 0;
  for (auto x : a) {
    ans += fw.query(vec.size()) - fw.query(index(x - T));
    fw.add(index(x), 1);
  }
  cout << ans << endl;
  return 0;
}
