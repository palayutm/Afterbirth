#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9 + 5;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n, 0);
  int cnt = 0;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    if (a[i] < 0) cnt++;
  }
  vector<int> v1, v2;
  int p = -1;
  if (cnt & 1) {
    int mx = -inf;
    for (int i = 0; i < n; i++) {
      if (a[i] < 0 && a[i] > mx) {
        mx = a[i];
        p = i;
      }
    }
  }
  for (int i = 0; i < n; i++) {
    if (a[i] == 0 || i == p) {
      v2.push_back(i + 1);
    } else {
      v1.push_back(i + 1);
    }
  }
  if (!v2.empty()) {
    for (int i = 0; i + 1 < v2.size(); i++) {
      cout << 1 << " " << v2[i] << " " << v2[i + 1] << endl;
    }
    if (!v1.empty()) {
      cout << 2 << " " << v2.back() << endl;
    }
  } 
  for (int i = 0; i + 1 < v1.size(); i++) {
    cout << 1 << " " << v1[i] << " " << v1[i + 1] << endl;
  }
  return 0;
}
