#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> f(1 << 3, inf);
  f[0] = 0;
  for (int i = 0; i < n; i++) {
    int x, v = 0;
    string s;
    cin >> x >> s;
    for (auto c : s) {
      v |= (1 << (c - 'A'));
    }
    for (int j = 7; j >= 0; j--) {
      f[j | v] = min(f[j | v], f[j] + x);
    }
  }
  cout << (f[7] == inf ? -1 : f[7]) << endl;
  return 0;
}
