#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  multiset<int> se;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    se.insert(a[i]);
  }
  sort(a.begin(), a.end());
  int ans = 0;
  for (auto x : a) {
    auto it = se.upper_bound(x);
    if (it != se.end()) {
      se.erase(it);
      ans++;
    }
  }
  cout << ans << endl;
  return 0;
}
