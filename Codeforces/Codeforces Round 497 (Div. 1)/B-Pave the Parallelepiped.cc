#include <bits/stdc++.h>

using namespace std;

vector<int> pr(100001);

inline int cal1(int x, int y, int z) {
  return pr[x] * pr[y] * pr[z];
}

inline int cal2(int x, int y, int z) {
  return pr[gcd(x, y)] * pr[z];
}

int main(int argc, char *argv[]) {
  for (int i = 1; i <= 100000; i++) {
    for (int j = i; j <= 100000; j += i) {
      pr[j]++;
    }
  }
  int T;
  scanf("%d", &T);
  while (T--) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    vector<vector<int>> vec = {{a, b, c}, {a, c, b}, {b, a, c}, {b, c, a}, {c, a, b}, {c, b, a}};
    long long ans = 0;
    for (int i = 1; i < 1 << 6; i++) {
      int cnt = 0;
      int x = -1, y = -1, z = -1;
      for (int j = 0; j < 6; j++) {
        if ((1 << j) & i) {
          cnt++;
          if (x == -1) x = vec[j][0], y = vec[j][1], z = vec[j][2];
          else x = gcd(vec[j][0], x), y = gcd(vec[j][1], y), z = gcd(vec[j][2], z);
        }
      }
      if (cnt & 1) ans += cal1(x, y, z) + 3 * cal2(x, y, z);
      else ans -= (cal1(x, y, z) + 3 * cal2(x, y, z));
    }
    printf("%lld\n", (ans + pr[gcd(gcd(a, b), c)] * 2) / 6);
  }
  return 0;
}
