#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  string s, t;
  cin >> n >> m >> s >> t;
  if (m + 1 < n) {
    cout << "NO" << endl;
    return 0;
  }
  int h = 0;
  for (int i = 0; i < n; i++) {
    if (s[i] == '*') {
      h = m - (n - i - 1);
    } else {
      if (h >= m || s[i] != t[h]) {
        cout << "NO" << endl;
        return 0;
      }
      h++;
    }
  }
  if (h != m) {
    cout << "NO" << endl;
  } else {
    cout << "YES" << endl;
  }
  return 0;
}
