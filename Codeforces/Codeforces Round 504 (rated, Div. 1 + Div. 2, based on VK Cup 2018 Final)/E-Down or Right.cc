#include <bits/stdc++.h>

using namespace std;

int n;

bool query(int x1, int y1, int x2, int y2) {
  if (x1 > n || y1 > n || x2 > n || y2 > n) return false;
  cout << "? " << x1 << " " << y1 << " " << x2 << " " << y2 << endl;
  string s;
  cin >> s;
  return s == "YES";
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  cin >> n;
  int x1 = 1, y1 = 1, x2 = n, y2 = n;
  string s1, s2;
  for (int i = 0; i < n - 1; i++) {
    if (query(x1 + 1, y1, n, n)) {
      s1 += "D";
      x1++;
    } else {
      y1++;
      s1 += "R";
    }
  }
  for (int i = 0; i < n - 1; i++) {
    if (query(1, 1, x2, y2 - 1)) {
      s2 += "R";
      y2--;
    } else {
      s2 += "D";
      x2--;
    }
  }
  reverse(s2.begin(), s2.end());
  cout << "! " << s1 << s2 << endl;
  return 0;
}
