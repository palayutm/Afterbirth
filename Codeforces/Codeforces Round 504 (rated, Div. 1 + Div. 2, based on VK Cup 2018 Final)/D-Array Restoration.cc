#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  auto check = [&]() {
    if (count(a.begin(), a.end(), m) == 0) {
      bool ok = false;
      for (int i = 0; i < n; i++) {
        if (a[i] == 0) {
          a[i] = m;
          ok = true;
          break;
        }
      }
      if (!ok) {
        return false;
      }
    }
    list<int> b;
    b.push_back(inf);
    vector<list<int>::iterator> a1(m + 1), a2(m + 1);
    vector<bool> vv(m + 1);
    for (int i = 0; i < n; i++) {
      if (a[i] == 0) {
        b.push_back(-i);
      } else {
        b.push_back(a[i]);
        auto it = b.end();
        --it;
        if (!vv[a[i]]) {
          a1[a[i]] = it;
          vv[a[i]] = true;
        }
        a2[a[i]] = it;
      }
    }
    for (int i = m; i >= 1; i--) {
      if (!vv[i]) continue;
      for (auto it = a1[i]; ;) {
        if (*it > 0 && *it < i) return false;
        if (*it <= 0) a[-*it] = i;
        bool ed = (it == a2[i]);
        it = b.erase(it);
        if (ed) break;
      }
    }
    return true;
  };
  if (check()) {
    cout << "YES" << endl;
    for (auto x : a) {
      if (x == 0) x = 1;
      cout << x << " ";
    }
  } else {
    cout << "NO" << endl;
  }
  return 0;
}
