#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, K;
  string s;
  cin >> n >> K >> s;
  K = n - K;
  vector<char> vec;
  for (auto c : s) {
    if (c == '(') {
      vec.push_back(c);
    } else {
      if (K) vec.pop_back(), K -= 2;
      else vec.push_back(c);
    }
  }
  for (auto c : vec) {
    cout << c;
  }
  return 0;
}
