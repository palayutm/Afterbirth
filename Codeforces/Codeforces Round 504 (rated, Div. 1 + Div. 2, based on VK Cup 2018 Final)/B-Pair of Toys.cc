#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  long long n, K;
  cin >> n >> K;
  long long L = max(1LL, K - n);
  long long R = min(n, (K - 1) / 2);
  R = max(R, L - 1);
  cout << R - L + 1 << endl;
  return 0;
}
