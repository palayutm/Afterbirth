#include <bits/stdc++.h>

using namespace std;

vector<vector<pair<int, int>>> g;
vector<int> dis(100005);
vector<pair<int, int>> fa(100005);

void dfs(int u) {
  for (auto [v, w] : g[u]) {
    if (dis[v] == -1) {
      dis[v] = dis[u] + w;
      fa[v] = {u, w};
      dfs(v);
    }
  }
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, K;
  cin >> n >> K;
  g.resize(n);
  for (int i = 0; i < n - 1; i++) {
    int u, v, w;
    cin >> u >> v >> w;
    g[u - 1].push_back({v - 1, w});
    g[v - 1].push_back({u - 1, w});
  }
  fill(dis.begin(), dis.end(), -1);
  dis[0] = 0;
  dfs(0);
  int u = 0;
  for (int i = 0; i < n; i++) {
    if (dis[i] > dis[u]) {
      u = i;
    }
  }
  fill(dis.begin(), dis.end(), -1);
  dis[u] = 0;
  dfs(u);
  int v = 0;
  for (int i = 0; i < n; i++) {
    if (dis[i] > dis[v]) {
      v = i;
    }
  }
  vector<pair<int, int>> a(1, {v, 0});
  int sum = 0;
  while (v != u) {
    a.push_back({fa[v].first, fa[v].second + a.back().second});
    sum += fa[v].second;
    v = fa[v].first;
  }
  fill(dis.begin(), dis.end(), -1);
  for (auto [v, w] : a) {
    dis[v] = 0;
  }
  for (auto [v, w] : a) {
    dfs(v);
  }
  int mx = 0;
  for (int i = 0; i < n; i++) {
    mx = max(mx, dis[i]);
  }
  int ans = 2e9;
  for (int i = 0; i < a.size(); i++) {
    int x = a[i].second;
    int y = sum - a[min(i + K - 1, (int)a.size() - 1)].second;
    ans = min(ans, max(x, y));
  }
  cout << max(mx, ans);
  return 0;
}
