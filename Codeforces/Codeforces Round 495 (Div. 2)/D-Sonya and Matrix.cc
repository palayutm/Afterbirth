#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int S;
  cin >> S;
  vector<int> a(S, 0);
  int d = 0;
  for (int i = 0, t; i < S; i++) {
    cin >> t;
    a[t]++;
    d = max(d, t);
  }
  int x = 1;
  for (int i = 0; i < S; i++) {
    if (a[i] < i * 4) {
      x = i;
      break;
    }
  }
  for (int n = 1; n <= S; n++) {
    if (S % n) continue;
    int m = S / n, y = n + m - d - x;
    vector<int> b(S, 0);
    if (x > n || y > m || y < 1) continue;
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        b[abs(i - x) + abs(j - y)]++;
      }
    }
    if (a == b) {
      cout << n << " " << m << endl;
      cout << x << " " << y << endl;
      return 0;
    }
  }
  cout << -1 << endl;
  return 0;
}
