#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cout << (i % 2 ? "1" : "0");
  }

  return 0;
}
