#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, d;
  cin >> n >> d;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  sort(a.begin(), a.end());
  int ans = 2;
  for (int i = 1; i < n; i++) {
    if (a[i] - a[i - 1] > 2 * d) ans += 2;
    if (a[i] - a[i - 1] == 2 * d) ans++;
  }
  cout << ans << endl;
  return 0;
}
