#include <bits/stdc++.h>

using namespace std;

template <class T> struct FenwickTree {
  std::vector<T> in;
  int N;
  FenwickTree(int N) : N(N), in(N) {}
  void add(int at, T by) {
    for (int i = at; i < N; i += (i & -i)) {
      in[i] += by;
    }
  }
  T query(int at) {
    T sum = 0;
    for (int i = at; i; i -= (i & -i)) {
      sum += in[i];
    }
    return sum;
  }
};

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(100001, n + 1);
  vector<int> b(100001, 0);
  for (int i = 1; i <= n; i++) {
    int t;
    cin >> t;
    a[t] = i;
    if (b[t] == 0) b[t] = i;
  }
  FenwickTree<int> ft(100001);
  for (int i = 1; i <= 100000; i++) {
    if (b[i] != 0) {
      ft.add(b[i], 1);
    }
  }
  long long ans = 0;
  for (int i = 1; i <= 100000; i++) {
    if (a[i] != n + 1) {
      ans += ft.query(a[i] - 1);
    }
  }
  cout << ans << endl;
  return 0;
}
