#include <bits/stdc++.h>
#include "../../Library/misc/fraction.hpp"

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, m, W;
    cin >> n >> m >> W;
    vector<string> vec(n);
    for (int i = 0; i < n; i++) {
      cin >> vec[i];
    }
    vector<pair<int, int>> a[n + 1][m + 1];
    auto add = [&](string s) {
      for (int i = 0; i < n; i++) {
        for (int j = s.size() - 1; j < m; j++) {
          bool ok = true;
          for (int k = 0; k < s.size(); k++) {
            ok &= (vec[i][j - k] == s[k]);
          }
          if (ok) {
            a[i][j].emplace_back(i, j + 1 - s.size());
          }
        }
      }
      for (int i = s.size() - 1; i < n; i++) {
        for (int j = 0; j < m; j++) {
          bool ok = true;
          for (int k = 0; k < s.size(); k++) {
            ok &= (vec[i - k][j] == s[k]);
          }
          if (ok) {
            a[i][j].emplace_back(i + 1 - s.size(), j);
          }
        }
      }
    };
    for (int i = 0; i < W; i++) {
      string s;
      cin >> s;
      add(s);
      reverse(s.begin(), s.end());
      add(s);
    }
    Fraction ans(0, 1);
    int cnt = 0;
    for (int x1 = 0; x1 < n; x1++) {
      for (int x2 = x1; x2 < n; x2++) {
        vector<int> sum(m);
        for (int y1 = 0; y1 < m; y1++) {
          for (int i = x1; i <= x2; i++) {
            for (auto [x, y] : a[i][y1]) {
              if (x >= x1) sum[y] += i - x + y1 - y + 1;
            }
          }
          for (int y2 = y1, now = 0; y2 >= 0; y2--) {
            now += sum[y2];
            Fraction fr(now, y1 - y2 + x2 - x1 + 2);
            if (ans < fr) {
              ans = fr, cnt = 1;
            } else if (ans == fr) {
              cnt++;
            }
          }
        }
      }
    }
    ans.reduction();
    printf("Case #%d: %lld/%lld %d\n", cas + 1, ans.a, ans.b, cnt);
  }
  return 0;
}
