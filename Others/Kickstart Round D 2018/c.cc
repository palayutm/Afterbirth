#include <bits/stdc++.h>
#include "../../Library/misc/fraction.hpp"

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, m, D;
    cin >> n >> m >> D;
    vector<string> vec(n);
    for (int i = 0; i < n; i++) {
      cin >> vec[i];
    }
    set<char> se;
    for (int i = 0; i < D; i++) {
      string s;
      cin >> s;
      se.insert(s[0]);
    }
    vector<vector<int>> a(n + 1, vector<int>(m + 1));
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        a[i][j] = a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1] + se.count(vec[i - 1][j - 1]) * 4;
      }
    }
    Fraction ans(0, 1);
    int cnt = 0;
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        for (int k = i; k <= n; k++) {
          for (int h = j; h <= m; h++) {
            int v = a[k][h] - a[i - 1][h] - a[k][j - 1] + a[i - 1][j - 1];
            Fraction now(v, k - i + h - j + 2);
            if (ans < now) ans = now, cnt = 1;
            else if (now == ans) {
              cnt++;
            }
          }
        }
      }
    }
    ans.reduction();
    printf("Case #%d: %lld/%lld %d\n", cas + 1, ans.a, ans.b, cnt);
  }
  return 0;
}
