#include <bits/stdc++.h>

using namespace std;

const long long inf = 1e18;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, O;
    long long D;
    cin >> n >> O >> D;
    vector<long long> X(n + 1, 0);
    long long A, B, C, M, L;
    cin >> X[1] >> X[2] >> A >> B >> C >> M >> L;
    for (int i = 3; i <= n; i++) {
      X[i] = (X[i - 1] * A + X[i - 2] * B + C) % M;
    }
    vector<long long> a(n + 1, 0);
    vector<int> b(n + 1);
    for (int i = 1; i <= n; i++) {
      a[i] = X[i] + L;
      if (a[i] & 1) {
        b[i]++;
      }
    }
    for (int i = 1; i <= n; i++) {
      a[i] += a[i - 1];
      b[i] += b[i - 1];
    }
    long long mx = -inf;
    set<pair<long long, int>> se;
    se.insert({0, 0});
    for (int i = 1; i <= n; i++) {
      while (true) {
        auto it = se.lower_bound({a[i] - D, 0});
        if (it != se.end() && b[i] - b[it->second] > O) se.erase(it);
        else {
          if (it != se.end()) {
            mx = max(mx, a[i] - a[it->second]);
          }
          se.insert({a[i], i});
          break;
        }
      }
    }
    printf("Case #%d: ", cas + 1);
    if (mx == -inf) {
      puts("IMPOSSIBLE");
    } else {
      printf("%lld\n", mx);
    }
  }
  return 0;
}
