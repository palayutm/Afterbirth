#include <bits/stdc++.h>

using namespace std;

const long long inf = 1e18;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, K;
    cin >> n >> K;
    vector<long long> p(n + 1), h(n + 1);
    long long A, B, C, M;
    cin >> p[1] >> p[2] >> A >> B >> C >> M;
    for (int i = 3; i <= n; i++) {
      p[i] = (p[i - 1] * A + p[i - 2] * B + C) % M + 1;
    }
    cin >> h[1] >> h[2] >> A >> B >> C >> M;
    for (int i = 3; i <= n; i++) {
      h[i] = (h[i - 1] * A + h[i - 2] * B + C) % M + 1;
    }
    vector<long long> x(K + 1), y(K + 1);
    cin >> x[1] >> x[2] >> A >> B >> C >> M;
    for (int i = 3; i <= K; i++) {
      x[i] = (x[i - 1] * A + x[i - 2] * B + C) % M + 1;
    }
    cin >> y[1] >> y[2] >> A >> B >> C >> M;
    for (int i = 3; i <= K; i++) {
      y[i] = (y[i - 1] * A + y[i - 2] * B + C) % M + 1;
    }
    vector<tuple<int, int, int>> vec;
    for (int i = 1; i <= n; i++) {
      vec.emplace_back(p[i], h[i], 0);
    }
    for (int i = 1; i <= K; i++) {
      vec.emplace_back(x[i], y[i], i);
    }
    set<int> se;
    sort(vec.begin(), vec.end());
    long long mx = -inf;
    for (auto [x, y, t] : vec) {
      if (t > 0) {
        if (y + x <= mx) {
          se.insert(t);
        }
      } else {
        mx = max(mx, 0LL + y + x);
      }
    }
    reverse(vec.begin(), vec.end());
    mx = -inf;
    for (auto [x, y, t] : vec) {
      if (t > 0) {
        if (y - x <= mx) {
          se.insert(t);
        }
      } else {
        mx = max(mx, 0LL + y - x);
      }
    }
    printf("Case #%d: %d\n", cas + 1, (int)se.size());
  }

  return 0;
}
