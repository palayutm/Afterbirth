#include <bits/stdc++.h>

using namespace std;

std::vector<int> preInverse(int n, int md) {
  std::vector<int> inv(n + 1);
  inv[1] = 1;
  for (int i = 2; i <= n; ++i) {
    inv[i] = (long long)(md - md / i) * inv[md % i] % md;
  }
  return inv;
}

const int mod = 1e9 + 7;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  auto inv = preInverse(10001, mod);
  int T;
  cin >> T;
  while (T--) {
    int n;
    cin >> n;
    vector<pair<int, int>> a(n);
    int mxl = 0, mxr = 0;
    for (int i = 0; i < n; i++) {
      cin >> a[i].first >> a[i].second;
      mxl = max(mxl, a[i].first);
      mxr = max(mxr, a[i].second);
    }
    auto cal = [&](long long x, long long y) {
      return (x + y) * (y - x + 1) % mod * inv[2] % mod;
    };
    long long ans = 0;
    for (int i = mxl, last = 0; i <= mxr; i++) {
      long long sum = 1;
      vector<long long> b(n + 1);
      b[n] = 1;
      for (int j = 0; j < n; j++) {
        int y = i - a[j].first + 1;
        int x = max(2, i - a[j].second + 1);
        b[j] = cal(x, y);
      }
      for (int j = n - 1; j >= 0; j--) {
        b[j] = (b[j] * b[j + 1]) % mod;
      }
      for (int j = 0; j < n; j++) {
        if (a[j].second >= i) {
          ans = (ans + sum * b[j + 1]) % mod;          
        }
        int y = i - a[j].first + 1;
        int x = max(1, i - a[j].second + 1);
        sum = sum * cal(x, y) % mod;
      }
    }
    for (int i = 0; i < n; i++) {
      ans = ans * inv[a[i].second - a[i].first + 1] % mod;
    }
    cout << ans << endl;
  }
  return 0;
}
