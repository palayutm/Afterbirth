#include <bits/stdc++.h>

using namespace std;

int a[21][21];
unordered_map<bitset<401>, int> b[21][21];

int main(int argc, char *argv[]) {
  int T;
  scanf("%d", &T);
  while (T--) {
    int n, m, K;
    scanf("%d%d%d", &n, &m, &K);
    queue<tuple<int, int, bitset<401>>> q;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        scanf("%d", &a[i][j]);
        b[i][j].clear();
        if (a[i][j] > 0) {
          bitset<401> t;
          t.set(a[i][j]);
          b[i][j][t] = 0;
          q.push(make_tuple(i, j, t));
        }
      }
    }
    auto solve = [&]() {
      int dx[] = {-1, 0, 0, 1};
      int dy[] = {0, -1, 1, 0};
      while (!q.empty()) {
        int x, y;
        bitset<401> t;
        tie(x, y, t) = q.front();
        int d = b[x][y][t];
        q.pop();
        for (int i = 0; i < 4; i++) {
          int tx = x + dx[i], ty = y + dy[i];
          if (tx >= 0 && tx < n && ty >= 0 && ty < m && a[tx][ty] >= 0) {
            auto tt = t;
            if (a[tx][ty] > 0) {
              tt.set(a[tx][ty]);
            }
            if (!b[tx][ty].count(tt)) {
              if (tt.count() >= K) {
                return d + 1;
              }
              b[tx][ty][tt] = d + 1;
              q.push(make_tuple(tx, ty, tt));
            }
          }
        }
      }
      return -1;
    };
    cout << solve() << endl;;
  }
  return 0;
}
