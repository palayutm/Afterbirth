#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;

vector<vector<vector<int>>> vec;
vector<vector<int>> b;

void dfs(int p, int v, vector<int>& now) {
  if (p == vec.size()) {
    b.push_back(now);
    return;
  }
  for (auto c : vec[p][v]) {
    now.push_back(c);
    dfs(p + 1, v, now);
    now.pop_back();
  }
}

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int T;
  cin >> T;
  while (T--) {
    int n, K;
    cin >> K >> n;
    b.clear();
    b.push_back(vector<int>(K, 0));
    vec = vector<vector<vector<int>>>(K, vector<vector<int>>(n + 1));
    vector<vector<int>> a(n);
    for (int i = 0; i < K; i++) {
      for (int j = 0; j < n; j++) {
        int t;
        cin >> t;
        vec[i][t].push_back(j + 1);
      }
    }
    vector<int> vv;
    for (int i = 1; i <= n; i++) {
      dfs(0, i, vv);
    }
    sort(b.begin(), b.end());
    long long ans = 0;
    vector<long long> f(b.size());
    f[0] = 1;
    for (int i = 1; i < b.size(); i++) {
      for (int j = 0; j < i; j++) {
        bool ok = true;
        for (int k = 0; k < K; k++) {
          if (b[i][k] <= b[j][k]) {
            ok = false;
            break;
          }
        }
        if (ok) {
          f[i] = (f[i] + f[j]) % mod;
        }
      }
      ans = (ans + f[i]) % mod;
    }
    cout << ans << endl;
  }
  return 0;
}
