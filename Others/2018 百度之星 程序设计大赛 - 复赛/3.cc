#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;

struct DisjointSet {
  std::vector<int> pre;

  DisjointSet(int n) : pre(n){
    for (int i = 0; i < n; i++) {
      pre[i] = i;
    }
  }
  int find(int u) { return pre[u] == u ? pre[u] : pre[u] = find(pre[u]); }
  void merge(int u, int v) { pre[find(u)] = find(v); }
};

int main(int argc, char *argv[]) {
  int T;
  scanf("%d", &T);
  while (T--) {
    int n, m;
    scanf("%d%d", &n, &m);
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
      scanf("%d", &a[i]);
    }
    DisjointSet ds(n);
    for (int i = 0; i < m; i++) {
      int u, v;
      scanf("%d%d", &u, &v);
      ds.merge(u - 1, v - 1);
    }
    vector<vector<int>> g(n);
    for (int i = 0; i < n; i++) {
      g[ds.find(i)].push_back(i);
    }
    long long ans = 0;
    for (int i = 0; i < n; i++) {
      vector<long long> b;
      for (auto v : g[i]) {
        b.push_back(a[v]);
      }
      sort(b.begin(), b.end());
      vector<int> c(33);
      for (int j = 0; j < b.size(); j++) {
        for (int k = 0; k < 30; k++) {
          if ((1 << k) & b[j]) {
            ans = (ans + b[j] * (1 << k) % mod * c[k]) % mod;
            c[k]++;
          }
        }
      }
    }
    cout << ans << endl;
  }
  return 0;
}
