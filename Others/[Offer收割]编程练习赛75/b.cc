#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  int n;
  cin >> n;
  vector<vector<int>> f(n + 1, vector<int>(n + 1, -inf));
  f[0][0] = 0;
  for (int i = 0; i < 3 * n; i++) {
    int x, y, z;
    cin >> x >> y >> z;
    vector<vector<int>> g(n + 1, vector<int>(n + 1));
    for (int j = 0; j < n + 1; j++) {
      for (int k = 0; k < n + 1; k++) {
        g[j][k] = f[j][k] + z;
        if (j > 0) g[j][k] = max(g[j][k], f[j - 1][k] + x);
        if (k > 0) g[j][k] = max(g[j][k], f[j][k - 1] + y);
      }
    }
    f = g;
  }
  cout << f[n][n] << endl;
  return 0;
}
