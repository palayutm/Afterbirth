#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int T;
  cin >> T;
  while (T--) {
    vector<int> a(100002);
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
      int t;
      cin >> t;
      a[t + 1]++;
    }
    auto solve = [&]() {
      vector<vector<int>> f(100002, vector<int>(4));
      for (int i = 1; i <= 100001; i++) {
        int t = a[i];
        if (t < f[i - 1][1]) return false;
        f[i][2] = f[i - 1][1];
        t -= f[i - 1][1];
        if (t < f[i - 1][2]) return false;
        f[i][3] = min(t, f[i - 1][2] + f[i - 1][3]);
        t -= f[i][3];
        f[i][1] = t;
      }
      return f[100001][1] == 0 && f[100001][2] == 0;
    };
    cout << (solve() ? "YES" : "NO") << endl;
  }

  return 0;
}
