#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  int n;
  cin >> n;
  vector<int> f(n + 1, -inf);
  f[0] = 0;
  for (int i = 0; i < 2 * n; i++) {
    int x, y;
    cin >> x >> y;
    vector<int> g(n + 1);
    for (int j = 0; j < n + 1; j++) {
      if (j > 0) g[j] = f[j - 1] + x;
      g[j] = max(g[j], f[j] + y);
    }
    f = g;
  }
  cout << f[n] << endl;
  return 0;
}
