#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  map<int, long long> key1, key2, mp1;
  map<pair<long long, long long>, int> mp;
  mp[{0, 0}] = 1;
  int n;
  cin >> n;
  long long now1 = 0, now2 = 0;
  long long ans = 0;
  for (int i = 0; i < n; i++) {
    int t;
    cin >> t;
    if (!key1.count(t)) {
      key1[t] = rand() + 1234;
      key2[t] = rand() + 3456;
    }
    if (mp1[t] == 0) {
      now1 += t * key1[t];
      now2 += t * key2[t];
    } else {
      now1 -= t * key1[t];
      now2 -= t * key2[t];
    }
    mp1[t] ^= 1;
    ans += mp[{now1, now2}];
    mp[{now1, now2}]++;
  }
  cout << ans << endl;
  return 0;
}
