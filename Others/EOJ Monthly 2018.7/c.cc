#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<pair<long long, long long>> a, b;
  for (int i = 0; i < n; i++) {
    int u, w;
    cin >> u >> w;
    if (w >= 0) {
      a.emplace_back(u, w);
    } else {
      b.emplace_back(u, w);
    }
  }
  sort(a.begin(), a.end());
  sort(b.begin(), b.end(), [](pair<long long, long long> x, pair<long long, long long> y) {
      return x.first + x.second > y.first + y.second;
    });
  long long ans = 1, now = 1;
  for (int i = 0; i < a.size(); i++) {
    if (now < a[i].first) {
      ans += a[i].first - now;
      now = a[i].first;
    }
    now += a[i].second;
  }
  for (int i = 0; i < b.size(); i++) {
    if (now < b[i].first) {
      ans += b[i].first - now;
      now = b[i].first;
    }
    now += b[i].second;
    if (now < 1) {
      ans += 1 - now;
      now = 1;
    }
  }
  cout << ans << endl;
  return 0;
}
