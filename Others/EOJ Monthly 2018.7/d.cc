#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  long long K;
  cin >> n >> K;
  vector<long long> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
    a[i] -= K * i;
  }
  sort(a.begin(), a.end());
  long long S1 = 0, S2 = 0;
  for (auto x : a) {
    S2 += x;
  }
  long long ans = 1e18;
  for (int i = 0; i < n; i++) {
    S1 += a[i];
    S2 -= a[i];
    if (a[i] >= 0 || (i + 1 < n && a[i + 1] >= 0)) {
      long long v = max(a[i], 0LL);
      ans = min(ans, (i + 1) * v - S1 + S2 - (n - i - 1) * v);
    }
  }
  cout << ans << endl;
  return 0;
}
