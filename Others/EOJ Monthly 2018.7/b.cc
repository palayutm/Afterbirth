#include <bits/stdc++.h>

using namespace std;

const double eps = 1e-8;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  long long S;
  cin >> S;
  if (S <= 2) {
    cout << "No" << endl;
    return 0;
  }
  if (S & 1) {
    cout << "Yes" << endl;
    cout << "1 0" << endl;
    cout << "0 1" << endl;
    cout << (S + 1) / 2 << " " << (S + 1) / 2 << endl;
  } else {
    cout << "Yes" << endl;
    cout << "0 0" << endl;
    cout << "0 2" << endl;
    cout << S / 2 << " 1"<< endl;
  }
  return 0;
}
