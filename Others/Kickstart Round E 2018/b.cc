#include <bits/stdc++.h>

using namespace std;

const int inf = 1e9;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 1; cas <= T; cas++) {
    int n, m, P;
    cin >> n >> m >> P;
    vector<vector<int>> a(P, vector<int>(2));
    for (int i = 0; i < n; i++) {
      string s;
      cin >> s;
      for (int j = 0; j < P; j++) {
        a[j][1 - (s[j] - '0')]++;
      }
    }
    vector<vector<int>> ch(1, vector<int>(2));
    vector<int> val(1);
    for (int i = 0; i < m; i++) {
      string s;
      cin >> s;
      int now = 0;
      for (auto c : s) {
        int v = c - '0';
        if (!ch[now][v]) {
          ch[now][v] = ch.size();
          ch.push_back(vector<int>(2));
          val.push_back(0);
        }
        now = ch[now][v];
      }
      val[now] = 1;
    }
    vector<vector<int>> dis(P + 1, vector<int>(ch.size(), inf));
    dis[0][0] = 0;
    for (int i = 0; i < P; i++) {
      for (int j = 0; j < ch.size(); j++) {
        dis[i + 1][ch[j][0]] = min(dis[i + 1][ch[j][0]], dis[i][j] + a[i][0]);
        dis[i + 1][ch[j][1]] = min(dis[i + 1][ch[j][1]], dis[i][j] + a[i][1]);
      }
    }
    int ans = inf;
    for (int i = 0; i < ch.size(); i++) {
      if (val[i] != 1) {
        ans = min(ans, dis[P][i]);        
      }
    }
    cout << "Case #" << cas << ": " << ans << endl;
  }
  return 0;
}
