#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 1; cas <= T; cas++) {
    int n, K;
    cin >> n >> K;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
    }
    sort(a.begin(), a.end());
    int ans = 0;
    for (int i = 0, h = 0; h < n; i++) {
      int cnt = 0;
      while (h < n && cnt < K) {
        if (a[h] > i) cnt++, ans++;
        h++;
      }
    }
    cout << "Case #" << cas << ": " << ans << endl;
  }
  return 0;
}
