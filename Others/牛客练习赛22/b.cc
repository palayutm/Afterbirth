#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  vector<int> a(3);
  for (int i = 0; i < 3; i++) {
    cin >> a[i];
  }
  int ans = max(a[0] * a[1] * a[2], max((a[0] + a[1]) * a[2], a[0] + a[1] + a[2]));
  ans = max(ans, a[0] * (a[1] + a[2]));
  ans = max(ans, a[0] * a[1] + a[2]);
  ans = max(ans, a[0] + a[1] * a[2]);
  cout << ans << endl;
  return 0;
}
