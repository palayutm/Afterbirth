#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  map<int, int> mp;
  for (int i = 0; i < 6; i++) {
    int t;
    cin >> t;
    mp[t]++;
  }
  vector<int> a;
  for (auto e : mp) {
    a.push_back(e.second);
  }
  sort(a.begin(), a.end());
  if (a[0] == 1 && a.back() >= 4) {
    cout << "Bear" << endl;
  } else if (a.size() == 1 || (a.size() == 2 && a[0] == 2)) {
    cout << "Elephant" << endl;
  } else {
    cout << "Hernia" << endl;
  }
  return 0;
}
