#include <bits/stdc++.h>
#include "../../../Library/misc/int_mod.hpp"
#include "../../../Library/misc/integer_util.hpp"

using namespace std;

int main(int argc, char *argv[]) {
  int T, cas = 1;
  cin >> T;
  while (T--) {
    int n, m;
    cin >> n >> m;
    vector<int> a(n + 1), b(n + 1), c(n + 1);
    for (int i = 1; i < n; i++) {
      cin >> a[i] >> b[i];
    }
    set<int> se;
    se.insert(0);
    for (int i = 0; i < m; i++) {
      int p, v;
      cin >> p >> v;
      c[p] = max(c[p], v);
      se.insert(v);
    }
    vector<int> vals(se.begin(), se.end());
    vals.push_back(1234567);
    for (int i = 1; i <= n; i++) {
      c[i] = lower_bound(vals.begin(), vals.end(), c[i]) - vals.begin();
    }
    vector<vector<IntMod>> f(n + 1, vector<IntMod>(vals.size()));
    auto g = f;
    if (c[1] == 0) {
      f[1][0] = 1;
    } else {
      g[1][c[1]] = 1;
    }
    auto inv = misc::inverse_table(1000001, 1e9 + 7);
    auto get = [&](int v, int p) -> IntMod{
      assert(p >= 1 && p <= n);
      if (v < a[p]) return 0;
      if (v > b[p]) return 1;
      return IntMod(v - a[p] + 1) * inv[b[p] - a[p] + 1];
    };
    for (int i = 1; i < n; i++) {
      IntMod S1 = 0;
      for (int j = 0; j < vals.size(); j++) {
        g[i + 1][max(j, c[i + 1])] += g[i][j] * get(vals[j], i);
        if (c[i + 1] == 0) {
          f[i + 1][0] += g[i][j] * (IntMod(1) - get(vals[j], i));
        } else {
          g[i + 1][c[i + 1]] += g[i][j] * (IntMod(1) - get(vals[j], i));
        }
        if (c[i + 1] > 0 && c[i + 1] >= j) {
          g[i + 1][c[i + 1]] += f[i][j] * get(vals[c[i + 1]], i);
        } else {
          f[i + 1][j] += f[i][j] * get(vals[j], i);
          if (j > 0) f[i + 1][j] += S1 * (get(vals[j], i) - get(vals[j - 1], i));
        }
        S1 += f[i][j];
      }
    }
    IntMod ans = 0;
    for (int i = 0; i < vals.size(); i++) {
      ans += f[n][i];
    }
    printf("Case #%d: %d\n", cas++, ans.val);
    fflush(stdout);
  }
  return 0;
}
