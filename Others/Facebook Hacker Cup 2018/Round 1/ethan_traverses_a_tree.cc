#include <bits/stdc++.h>
#include "../../../Library/data-structure/disjoint_set.hpp"

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, K;
    cin >> n >> K;
    vector<vector<int>> g(n, vector<int>(2));
    for (int i = 0; i < n; i++) {
      cin >> g[i][0] >> g[i][1];
      g[i][0]--, g[i][1]--;
    }
    function<void(int, vector<int>&)> dfs1 = [&](int u, vector<int>& seq) {
      seq.push_back(u);
      if (g[u][0] != -1) dfs1(g[u][0], seq);
      if (g[u][1] != -1) dfs1(g[u][1], seq);
    };
    function<void(int, vector<int>&)> dfs2 = [&](int u, vector<int>& seq) {
      if (g[u][0] != -1) dfs2(g[u][0], seq);
      if (g[u][1] != -1) dfs2(g[u][1], seq);
      seq.push_back(u);
    };

    DisjointSet ds(n);
    int sum = n;
    vector<int> seq1, seq2;
    dfs1(0, seq1);
    dfs2(0, seq2);
    for (int i = 0; i < n; i++) {
      int fa = ds.find(seq1[i]), fb = ds.find(seq2[i]);
      if (fa != fb) {
        sum--;
        ds.merge(fa, fb);
      }
    }
    printf("Case #%d:", cas + 1);
    if (sum < K) {
      puts(" Impossible");
    } else {
      map<int, int> mp;
      for (int i = 0; i < n; i++) {
        int fa = ds.find(i);
        if (!mp.count(fa)) {
          mp.insert({fa, min((int)mp.size() + 1, K)});
        }
        cout << ' ' << mp[fa];
      }
      cout << endl;
    }
  }
  return 0;
}
