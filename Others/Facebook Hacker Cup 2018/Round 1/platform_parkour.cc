#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n, m;
    cin >> n >> m;
    vector<long long> H(n + 1);
    long long W, X, Y, Z;
    cin >> H[1] >> H[2] >> W >> X >> Y >> Z;
    for (int i = 3; i <= n; i++) {
      H[i] = (W * H[i - 2] + X * H[i - 1] + Y) % Z;
    }
    vector<tuple<int, int, int, int>> vec;
    for (int i = 0; i < m; i++) {
      int a, b, u, d;
      cin >> a >> b >> u >> d;
      if (a > b) swap(a, b), swap(u, d);
      vec.emplace_back(a, b, u, d);
    }
    auto check = [&](double L) {
      vector<pair<double, double>> a(n + 1);
      for (int i = 1; i <= n; i++) {
        a[i] = {H[i] + L, H[i] - L};
      }
      for (int i = 1; i <= n; i++) {
        if (a[i].first < a[i].second) return false;
        for (auto [l, r, u, d] : vec) {
          if (l <= i && r > i) {
            a[i + 1].first = min(a[i + 1].first, a[i].first + u);
            a[i + 1].second = max(a[i + 1].second, a[i].second - d);
          }
        }
      }
      return true;
    };
    double low = 0, high = 1000000;
    for (int it = 0; it < 53; it++) {
      double mid = (low + high) / 2;
      if (check(mid)) high = mid;
      else low = mid;
    }
    printf("Case #%d: %.6f\n", cas + 1, low);
  }
  return 0;
}
