#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n;
    cin >> n;
    vector<string> vec;
    for (int i = 0; i < 3; i++) {
      string s;
      cin >> s;
      vec.push_back(s);
    }
    vector<vector<long long>> f(n + 1, vector<long long>(3));
    f[0][0] = 1;
    for (int i = 1; i <= n; i++) {
      if (vec[0][i - 1] != '#' && vec[1][i - 1] != '#') {
        f[i][0] = f[i - 1][1];
        f[i][1] += f[i - 1][0];
      }
      if (vec[2][i - 1] != '#' && vec[1][i - 1] != '#') {
        f[i][2] = f[i - 1][1];
        f[i][1] += f[i - 1][2];
      }
      f[i][1] %= mod;
    }
    printf("Case #%d: %lld\n", cas + 1, f[n][2]);
  }
  return 0;
}
