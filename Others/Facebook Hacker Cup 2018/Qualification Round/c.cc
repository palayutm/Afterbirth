#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    string s;
    cin >> s;
    auto solve = [](string s) -> string {
      vector<int> fail(s.size() + 1);
      vector<vector<int>> f(s.size() + 1, vector<int>(26));
      int now = 0;
      for (int i = 0; i < s.size(); i++) {
        for (int j = 0; j < 26; j++) {
          if (s[i] - 'A' == j) {
            fail[i + 1] = f[fail[i]][j];
            f[i][j] = i + 1;
          } else {
            f[i][j] = f[fail[i]][j];
            if (f[i][j] > 1) {
              return s.substr(0, i) + (char)(j + 'A') + s.substr(f[i][j]);
            }
          }
        }
      }
      return "Impossible";
    };
    cout << "Case #" << cas + 1 << ": " << solve(s) << endl;
  }
  return 0;
}
