#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T, cas = 1;
  cin >> T;
  while (T--) {
    int n, k;
    long long v;
    cin >> n >> k >> v;
    vector<string> a(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
    }
    int st = k * (v - 1) % n;
    cout << "Case #" << cas++ << ":";
    vector<int> b;
    for (int i = 0; i < k; i++) {
      b.push_back((st + i) % n);
    }
    sort(b.begin(), b.end());
    for (auto x : b) {
      cout << " " << a[x];
    }
    cout << endl;
  }
  return 0;
}
