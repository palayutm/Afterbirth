#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  for (int cas = 0; cas < T; cas++) {
    int n;
    cin >> n;
    vector<int> a(n + 1);
    for (int i = 0; i < n + 1; i++) {
      cin >> a[i];
    }
    printf("Case #%d: ", cas + 1);
    if (n & 1) {
      cout << "1\n0.0" << endl;
    } else {
      cout << 0 << endl;
    }
  }
  return 0;
}
