#include <bits/stdc++.h>

using namespace std;
vector<int> in(100000);
bool toposort(vector<vector<int>> g) {
  int n = g.size();
  fill(in.begin(), in.begin() + n, 0);
  for (int i = 0; i < n; ++i) {
    for (auto x: g[i]) {
      in[x]++;
    }
  }
  queue<int> q;
  for (int i = 0; i < n; ++i) {
    if (in[i] == 0) {
      q.push(i);
    }
  }
  int cnt = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    cnt++;
    for (auto v: g[u]) {
      if (--in[v] == 0) {
        q.push(v);
      }
    }
  }
  return cnt == n;
}

int main(int argc, char *argv[]) {
  int n, m;
  scanf("%d%d", &n, &m);
  vector<pair<int, int>> a;
  for (int i = 0; i < m; i++) {
    int x, y;
    scanf("%d%d", &x, &y);
    a.emplace_back(x, y);
  }
  int low = 0, high = m - 1;
  while (low <= high) {
    int mid = (low + high) / 2;
    int pre = 0;
    vector<vector<int>> g(n);
    for (int i = 0; i <= mid; i++) {
      int x = a[i].first, y = a[i].second;
      x = (x - 1 - pre + n) % n;
      y = (y - 1 - pre + n) % n;
      if (x == 0) x = n;
      if (y == 0) y = n;
      g[x - 1].push_back(y - 1);
      pre = 1;
    }
    if (toposort(g)) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }
  for (int i = 0; i < low; i++) {
    puts("1");
  }
  for (int i = low; i < m; i++) {
    puts("0");
  }
  return 0;
}
