#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<int> a(n + 1, 0);
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
  }
  vector<long long> f(n + 1);
  f[0] = 1;
  long long ans = 0;
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j < i; j++) {
      if (j == 0 || log(a[i]) / i > log(a[j]) / (j)) {
        f[i] = (f[i] + f[j]) % mod;
      }
    }
    ans = (ans + f[i]) % mod;
  }
  cout << ans << endl;
  return 0;
}
