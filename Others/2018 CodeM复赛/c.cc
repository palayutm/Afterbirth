#include <bits/stdc++.h>

using namespace std;

const int mod = 998244353;
vector<pair<int, int>> g[100001];

struct DisjointSet {
  std::vector<int> pre;

  DisjointSet(int n) : pre(n){
    for (int i = 0; i < n; i++) {
      pre[i] = i;
    }
  }
  int find(int u) { return pre[u] == u ? pre[u] : pre[u] = find(pre[u]); }
  void merge(int u, int v) { pre[find(u)] = find(v); }
};

int st[100001];
bool ok = true;

void dfs(int u) {
  for (auto e : g[u]) {
    int v = e.first, w = e.second;
    if (st[v] == -1) {
      st[v] = (st[u] ^ w);
      dfs(v);
    } else if (st[v] ^ w ^ st[u]) {
      ok = false;
    }
  }
}

int main(int argc, char *argv[]) {
  int n, m;
  scanf("%d%d", &n, &m);
  vector<pair<int, int>> a;
  DisjointSet ds(n + 1);
  for (int i = 1; i <= m; i++) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    if (w != -1) {
      g[u].emplace_back(v, w);
      g[v].emplace_back(u, w);
      ds.merge(u, v);
    } else {
      a.emplace_back(u, v);
    }
  }
  memset(st, -1, sizeof(st));
  for (int i = 1; i <= n; i++) {
    if (st[i] == -1) {
      st[i] = 0;
      dfs(i);
    }
  }
  if (!ok) {
    cout << 0 << endl;
    return 0;
  }
  long long ans = 1;
  for (auto e : a) {
    int u = e.first, v = e.second;
    if (ds.find(u) != ds.find(v)) {
      ds.merge(u, v);
      ans = (ans * 2) % mod;
    }
  }
  cout << ans << endl;
  return 0;
}
