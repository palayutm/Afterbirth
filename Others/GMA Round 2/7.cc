#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n = 100000000;
  vector<bool> a(n + 1);
  vector<int> ans(n + 1);
  for (int i = 2; i <= n; i++) {
    if (!a[i]) {
      for (int j = i; j <= n; j += i) {
        a[j] = true;
        int t = j;
        while (t % i == 0) {
          ans[j] ^= 1;
          t /= i;
        }
      }
    }
  }
  int cnt = 1;
  for (int i = 2; i <= n; i++) {
    cnt += (ans[i] == 0);
  }
  cout << cnt << endl;
  return 0;
}
