#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  double n = 100000000;
  double eps = 1e-5;
  for (int i = 1; ; i++) {
    n *= 0.9;
    if (n < eps) {
      cout << i << endl;
      break;
    }
  }
  return 0;
}
