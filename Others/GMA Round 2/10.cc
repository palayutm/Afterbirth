#include <bits/stdc++.h>

using namespace std;

const int mod = 1e9 + 7;

int main(int argc, char *argv[]) {
  int n = 1000000;
  vector<long long> a(n, 0);
  a[0] = 1;
  for (int i = 1; i < n; i++) {
    a[i] = (a[i - 1] * 1706167 + 1534823) % mod;
  }
  vector<int> b[40][40];
  for (auto x : a) {
    int cx = 0, cy = 0, ox = x;
    while (x % 2 == 0) {
      x /= 2;
      cx++;
    }
    while (x % 5 == 0) {
      x /= 5;
      cy++;
    }
    b[cx][cy].push_back(ox);
  }
  vector<int> vec;
  for (int i = 0; i < 40; i++) {
    for (int j = 0; j < 40; j++) {
      sort(b[i][j].begin(), b[i][j].end(), greater<int>());
      for (int k = 0; k < b[i][j].size() && k < 3; k++) {
        vec.push_back(b[i][j][k]);
      }
    }
  }
  __int128 ans = 0;
  cout << vec.size() << endl;
  for (int i = 0; i < vec.size(); i++) {
    for (int j = i + 1; j < vec.size(); j++) {
      for (int k = j + 1; k < vec.size(); k++) {
        __int128 now = vec[i];
        now *= 1LL * vec[j] * vec[k];
        while (now && now % 10 == 0) now /= 10;
        ans = max(ans, now);
      }
    }
  }
  string s;
  while (ans) {
    s += (char)(ans % 10 + '0');
    ans /= 10;
  }
  reverse(s.begin(), s.end());
  cout << s << endl;
  return 0;
}
