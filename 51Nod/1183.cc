#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  string s, t;
  cin >> s >> t;
  vector<vector<int>> f(s.size() + 1, vector<int>(t.size() + 1, 12345678));
  f[0][0] = 0;
  for (int i = 0; i < s.size(); i++) {
    f[i + 1][0] = i + 1;
  }
  for (int i = 0; i < t.size(); i++) {
    f[0][i + 1] = i + 1;
  }
  for (int i = 1; i <= s.size(); i++) {
    for (int j = 1; j <= t.size(); j++) {
      f[i][j] = min(f[i - 1][j], f[i][j - 1]) + 1;
      f[i][j] = min(f[i][j], f[i - 1][j - 1] + (s[i - 1] != t[j - 1]));
    }
  }
  cout << f[s.size()][t.size()] << endl;
  return 0;
}
