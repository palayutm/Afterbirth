#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  while (T--) {
    int a, b;
    cin >> a >> b;
    if (a > b) swap(a, b);
    cout << (a == (int)((b - a) * (1 + sqrt(5)) / 2) ? "B" : "A") << endl;
  }
  return 0;
}
