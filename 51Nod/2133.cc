#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n;
  cin >> n;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  sort(a.begin(), a.end());
  long long ans = 0, now = 0;
  for (auto x : a) {
    now += x;
    ans += now;
  }
  cout << ans << endl;
  return 0;
}
