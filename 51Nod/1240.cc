#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, cnt = 0;
  cin >> n;
  for (int i = 2; i * i <= n; i++) {
    if (n % i == 0) {
      n /= i;
      cnt ^= 1;
      if (n % i == 0) {
        cout << 0 << endl;
        return 0;
      }
    }
  }
  if (n != 1) cnt ^= 1;
  cout << (!cnt ? 1 : -1) << endl;
  return 0;
}
