import decimal
decimal.getcontext().prec = 100
d = (1 + decimal.Decimal(5) ** decimal.Decimal(0.5)) / 2
T = int(input())
while T > 0:
    T -= 1
    a, b = map(int, input().split())
    if a > b:
        a, b = b, a
    if a == int((b - a) * d):
        print('B')
    else:
        print('A')
    
