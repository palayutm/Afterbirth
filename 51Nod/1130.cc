#include <bits/stdc++.h>

using namespace std;

const double pi = acos(-1);

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  while (T--) {
    int n;
    cin >> n;
    cout << (long long)((log(sqrt(2 * pi * n)) + n * (log(n / exp(1)))) / log(10)) + 1 << endl;
  }
  return 0;
}
