#include <bits/stdc++.h>

using namespace std;

int det(vector<vector<int>> a) {
  long long ret = 1;
  int n = a.size();
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n; j++) {
      while (a[j][i] != 0) {
        long long t = a[i][i] / a[j][i];
        for (int k = i; k <= n; k++) {
          a[i][k] = a[i][k] - a[j][k] * t;
        }
        swap(a[i], a[j]);
        ret *= -1;
      }
    }
    ret = ret * a[i][i];
  }
  return ret;
}

int main(int argc, char *argv[]) {
  int T;
  cin >> T;
  while (T--) {
    vector<vector<int>> a;
    int x1, y1, z1;
    for (int i = 0; i < 4; i++) {
      if (i == 0) cin >> x1 >> y1 >> z1;
      else {
        int x, y, z;
        cin >> x >> y >> z;
        x -= x1, y -= y1, z -= z1;
        a.push_back({x, y, z});
      }
    }
    cout << (det(a) == 0 ? "Yes" : "No") << endl;
  }
  return 0;
}
