#include <bits/stdc++.h>

using namespace std;

namespace graph {
typedef long long T;
const T INF = 1e18;
typedef std::vector<std::vector<std::pair<int, int>>> Graph;
std::vector<T> spfa(const Graph &g, int src) {
  int n = g.size();
  std::vector<long long> distance(n, INF);
  std::vector<bool> in_queue(n);
  std::queue<int> q;
  q.push(src);
  distance[src] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    in_queue[u] = false;
    for (auto e : g[u]) {
      int v = e.first, w = e.second;
      if (distance[u] + w < distance[v]) {
        distance[v] = distance[u] + w;
        if (!in_queue[v]) {
          in_queue[v] = true;
          q.push(v);
        }
      }
    }
  }
  return distance;
}
} // namespace graph

class TeleportationMaze {
public:
  int pathLength(vector<string> a, int r1, int c1, int r2, int c2) {
    int n = a.size(), m = a[0].size();
    vector<vector<pair<int, int>>> g(n * m);
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (a[i][j] == '.') {
          for (int k = i + 1; k < n; k++) {
            if (a[k][j] == '.') {
              g[i * m + j].emplace_back(k * m + j, k == i + 1 ? 1 : 2);
              g[k * m + j].emplace_back(i * m + j, k == i + 1 ? 1 : 2);
              break;
            }
          }
          for (int k = j + 1; k < m; k++) {
            if (a[i][k] == '.') {
              g[i * m + k].emplace_back(i * m + j, k == j + 1 ? 1 : 2);
              g[i * m + j].emplace_back(i * m + k, k == j + 1 ? 1 : 2);
              break;
            }
          }
        }
      }
    }
    long long d = graph::spfa(g, r1 * m + c1)[r2 * m + c2];
    if (d == graph::INF) d = -1;
    return d;
  }
};

// CUT begin
ifstream data("TeleportationMaze.sample");

string next_line() {
    string s;
    getline(data, s);
    return s;
}

template <typename T> void from_stream(T &t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string &s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T> &ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

bool do_test(vector<string> a, int r1, int c1, int r2, int c2, int __expected) {
    time_t startClock = clock();
    TeleportationMaze *instance = new TeleportationMaze();
    int __result = instance->pathLength(a, r1, c1, r2, c2);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int> &case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        vector<string> a;
        from_stream(a);
        int r1;
        from_stream(r1);
        int c1;
        from_stream(c1);
        int r2;
        from_stream(r2);
        int c2;
        from_stream(c2);
        next_line();
        int __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if ( do_test(a, r1, c1, r2, c2, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1531558268;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 600 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if ( string(argv[i]) == "-") {
            mainProcess = false;
        } else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "TeleportationMaze (600 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
