#include <bits/stdc++.h>

using namespace std;

class MaxSquare {
public:
  struct A {
    long long x, y, id;
    A(long long a, long long b, long long c) : x(a), y(b), id(c) {}
  };
  long long getMaxSum(int n, long long s, int q, int o, vector<int> x, vector<int> y) {
    vector<long long> B(n);
    for (int i = 0; i < n; i++) {
      B[i] = (s / (1 << 20)) % q + o;
      long long s0 = s * 621 % (1LL << 51);
      long long s1 = s * 825 % (1LL << 51);
      long long s2 = s * 494 % (1LL << 51);
      long long s3 = s * 23 % (1LL << 51);
      s = s3;
      s = (s * (1 << 10) + s2) % (1LL << 51);
      s = (s * (1 << 10) + s1) % (1LL << 51);
      s = (s * (1 << 10) + s0 + 11) % (1LL << 51);
    }
    for (int i = 0; i < x.size(); i++) {
      B[x[i]] = y[i];
    }
    bool ok = true;
    for (int i = 0; i < n; i++) {
      ok &= (B[i] < 0);
    }
    if (ok) {
      long long mx = -1e18;
      for (auto x : B) {
        mx = max(mx, x);
      }
      return mx * 2;
    }
    vector<A> query, cand;
    query.push_back(A(-1, 0, 0));
    cand.push_back(A(-1, 0, 0));
    for (int i = 0; i < n; i++) {
      if (i) B[i] = B[i - 1] + B[i];
      if (B[i] < query.back().y) {
        query.push_back(A(i, B[i], query.size()));
      }
      while (!cand.empty() && B[i] >= cand.back().y) cand.pop_back();
      cand.push_back(A(i, B[i], cand.size()));
    }
    vector<int> opt(query.size());
    solve(query, cand, opt);
    long long mx = 0;
    for (int i = 0; i < query.size(); i++) {
      int t = opt[i];
      mx = max(mx, (cand[t].x - query[i].x) * (cand[t].y - query[i].y));
    }
    return mx * 2;
  }

  void solve(vector<A> query, vector<A> cand, vector<int>& opt) {
    if (query.empty()) return;
    int m = query.size() / 2;
    int p = -1;
    long long mx = -1;
    for (int i = 0; i < cand.size(); i++) {
      auto e = cand[i];
      if (query[m].y <= e.y) {
        long long v = (e.y - query[m].y) * (e.x - query[m].x);
        if (v > mx) {
          mx = v;
          p = i;
        }
      }
    }
    opt[query[m].id] = cand[p].id;
    solve({query.begin(), query.begin() + m}, {cand.begin(), cand.begin() + p + 1}, opt);
    solve({query.begin() + m + 1, query.end()}, {cand.begin() + p, cand.end()}, opt);
  }
};

// CUT begin
ifstream data("MaxSquare.sample");

string next_line() {
    string s;
    getline(data, s);
    return s;
}

template <typename T> void from_stream(T &t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string &s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T> &ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

bool do_test(int n, long long s, int q, int o, vector<int> x, vector<int> y, long long __expected) {
    time_t startClock = clock();
    MaxSquare *instance = new MaxSquare();
    long long __result = instance->getMaxSum(n, s, q, o, x, y);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int> &case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        int n;
        from_stream(n);
        long long s;
        from_stream(s);
        int q;
        from_stream(q);
        int o;
        from_stream(o);
        vector<int> x;
        from_stream(x);
        vector<int> y;
        from_stream(y);
        next_line();
        long long __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if ( do_test(n, s, q, o, x, y, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1531745744;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 1000 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if ( string(argv[i]) == "-") {
            mainProcess = false;
        } else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "MaxSquare (1000 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
