#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int T;
  cin >> T;
  while (T--) {
    long long a, b, c, d;
    cin >> a >> b >> c >> d;
    auto cal = [](long long a, long long b, long long c, long long d) {
      if (a < b) return false;
      if (b > d) return false;
      long long g = __gcd(b, d);
      long long t1 = c - a % g, t2 = b - a % g - 1;
      long long c1 = (t1 < 0 ? -1 : t1 / g);
      if (t2 < 0) return true;
      return t2 / g <= c1;
    };
    cout << (cal(a, b, c, d) ? "Yes" : "No") << endl;
  }
  return 0;
}
