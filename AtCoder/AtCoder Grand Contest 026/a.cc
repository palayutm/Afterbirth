#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  int ans = 0;
  for (int i = 0, last = -1; i < n; i++) {
    int t;
    cin >> t;
    if (t == last) {
      t = -1;
      ans++;
    }
    last = t;
  }
  cout << ans << endl;
  return 0;
}
