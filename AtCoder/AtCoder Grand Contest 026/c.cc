#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  string s;
  cin >> n >> s;
  map<pair<string, string>, int> mp;
  for (int i = 0; i < 1 << n; i++) {
    string x, y;
    for (int j = 0; j < n; j++) {
      if ((1 << j) & i) {
        x += s[j];
      } else {
        y += s[j];
      }
    }
    reverse(y.begin(), y.end());
    mp[{x, y}]++;
  }
  long long ans = 0;
  for (int i = 0; i < 1 << n; i++) {
    string x, y;
    for (int j = 0; j < n; j++) {
      if ((1 << j) & i) {
        x += s[2 * n - j - 1];
      } else {
        y += s[2 * n - j - 1];
      }
    }
    reverse(y.begin(), y.end());
    ans += mp[{x, y}];
  }
  cout << ans << endl;
  return 0;
}
