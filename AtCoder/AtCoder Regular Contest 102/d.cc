#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int L;
  cin >> L;
  vector<tuple<int, int, int>> edge;
  for (int i = 19; i >= 2; i--) {
    edge.emplace_back(i, i + 1, 0);
    edge.emplace_back(i, i + 1, 1 << (20 - i - 1));
  }
  int now = 0;
  if ((1 << 19) & L) {
    now |= (1 << 19);
    edge.emplace_back(1, 2, 0);
    edge.emplace_back(1, 2, 1 << 18);
  }
  for (int i = 2; i <= 20; i++) {
    if ((1 << (20 - i)) & L) {
      edge.emplace_back(1, i, now);
      now |= (1 << (20 - i));
    }
  }
  cout << 20 << " " << edge.size() << endl;
  for (auto e : edge) {
    int u, v, w;
    tie(u, v, w) = e;
    cout << u << " " << v << " " << w << endl;
  }
  return 0;
}
