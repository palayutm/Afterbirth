#include <bits/stdc++.h>

using namespace std;

int powMod(int x, int y, int z) {
  long long ret = 1;
  for (; y; y >>= 1, x = (long long)x * x % z) {
    if (y & 1)
      ret = ret * x % z;
  }
  return ret;
}

const int mod = 998244353;

long long C[2222][2222];
long long f[2222][2222];

int main(int argc, char *argv[]) {
  int K, n;
  cin >> K >> n;
  for (int i = 0; i <= 2000; i++) {
    C[i][0] = C[i][i] = 1;
    for (int j = 1; j < i; j++) {
      C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % mod;
    }
  }
  f[0][0] = 1;
  for (int i = 1; i <= 2000; i++) {
    f[i][0] = f[i - 1][0];
    for (int j = 1; j <= 2000; j++) {
      f[i][j] = (f[i - 1][j] + f[i][j - 1]) % mod;
    }
  }
  for (int i = 2; i <= 2 * K; i++) {
    int extra = 0, s1 = 0, s2 = 0;
    for (int j = 1; j <= K; j++) {
      if (j * 2 == i) {
        extra++;
      } else if (i - j <= K && i - j > 0) {
        s1 += (i - j < j);
      } else {
        s2++;
      }
    }
    auto solve = [&](int n, int s1, int s2) {
      long long ans = 0;
      for (int i = 0; i <= s1 && i <= n; i++) {
        ans = (ans + C[s1][i] * f[s2 + i][n - i] % mod * powMod(2, i, mod)) % mod;
      }
      return ans;
    };
    cout << (solve(n, s1, s2) + (extra ? solve(n - 1, s1, s2) : 0 )) % mod << endl;
  }
  return 0;
}
