#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  auto cal = [](long long n) {
    return n * n * n;
  };
  long long n, K;
  cin >> n >> K;
  long long ans = cal(n / K);
  if (!(K & 1)) {
    ans += cal(n / K + (n % K >= K / 2));
  }
  cout << ans << endl;
  return 0;
}
