#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m;
  string s;
  cin >> n >> m >> s;
  vector<int> a(n);
  for (int i = 0; i < n; i++) {
    a[i] = s[i] == 'A';
  }
  vector<vector<int>> b(n, vector<int>(2));
  vector<vector<int>> g(n);
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    g[u].push_back(v);
    g[v].push_back(u);
    b[u][a[v]]++;
    b[v][a[u]]++;
  }
  vector<bool> vis(n);
  queue<int> q;
  for (int i = 0; i < n; i++) {
    if (!b[i][0] || !b[i][1]) {
      q.push(i);
    }
  }
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = true;
    for (auto v : g[u]) {
      if (!vis[v]) {
        b[v][a[u]]--;
        if (b[v][a[u]] == 0) {
          q.push(v);
        }
      }
    }
  }
  bool ok = true;
  for (auto x : vis) {
    ok &= x;
  }
  cout << (!ok ? "Yes" : "No") << endl; 
  return 0;
}
