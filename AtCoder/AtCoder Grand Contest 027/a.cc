#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, X;
  cin >> n >> X;
  vector<int> a(n, 0);
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  sort(a.begin(), a.end());
  int ans = 0;
  for (int i = 0; i + 1 < a.size(); i++) {
    if (X >= a[i]) {
      X -= a[i];
      ans++;
    }
  }
  ans += X == a.back();
  cout << ans << endl;
  return 0;
}
