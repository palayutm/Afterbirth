#include <bits/stdc++.h>

using namespace std;

template <class T> struct FenwickTree {
  std::vector<T> in;
  int N;
  FenwickTree(int N) : N(N), in(N) {}
  void update(int at, T by) {
    for (int i = at; i < N; i += (i & -i)) {
      in[i] += by;
    }
  }
  T query(int at) {
    T sum = 0;
    for (int i = at; i; i -= (i & -i)) {
      sum += in[i];
    }
    return sum;
  }
};

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, A;
  cin >> n >> A;
  vector<vector<int>> g(1000001);
  for (int i = 0, x, y; i < n; i++) {
    cin >> x >> y;
    g[x].push_back(y);
  }
  FenwickTree<int> ft(1000001);
  int ans = 0;
  for (int i = 0; i <= 1000000; i++) {
    for (auto x : g[i]) {
      ft.update(x, 1);
    }
    int p = (i == 0 ? 1000000 : A / i);
    ans = max(ans, ft.query(min(p, 1000000)));
  }
  cout << ans << endl;
  return 0;
}
