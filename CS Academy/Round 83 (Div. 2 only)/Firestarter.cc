#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, m, Q;
  cin >> n >> m >> Q;
  vector<vector<pair<int, int>>> g(n + 1);
  map<pair<int, int>, int> el;
  map<pair<int, int>, vector<pair<int, int>>> ed;
  for (int i = 0; i < m; i++) {
    int u, v, w;
    cin >> u >> v >> w;
    g[u].emplace_back(v, w);
    g[v].emplace_back(u, w);
    el[{u, v}] = w;
    el[{v, u}] = w;
  }
  for (int i = 0; i < Q; i++) {
    int t, a, b, x;
    cin >> t >> a >> b >> x;
    ed[{a, b}].push_back({x, t});
    ed[{b, a}].push_back({-x + el[{b, a}], t});
    g[0].push_back({a, t + x});
    g[0].push_back({b, -x + el[{b, a}] + t});
  }
  vector<long long> dis(n + 1, 1e18);
  vector<bool> vis(n + 1);
  queue<int> q;
  q.push(0);
  dis[0] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = false;
    for (auto e : g[u]) {
      int v = e.first, w = e.second;
      if (dis[u] + w < dis[v]) {
        dis[v] = dis[u] + w;
        if (!vis[v]) {
          vis[v] = true;
          q.push(v);
        }
      }
    }
  }
  long long ans = 0;
  for (int i = 1; i <= n; i++) {
    for (auto e : g[i]) {
      int v = e.first, L = e.second;
      vector<pair<int, long long>> a;
      a.push_back({0, dis[i]});
      a.push_back({L, dis[v]});
      for (auto e2 : ed[{i, v}]) {
        a.push_back(e2);
      }
      sort(a.begin(), a.end());
      for (int j = 1; j < a.size(); j++) {
        a[j].second = min(a[j].second, a[j - 1].second + a[j].first - a[j - 1].first);
      }
      for (int j = a.size() - 2; j >= 0; j--) {
        a[j].second = min(a[j].second, a[j + 1].second - a[j].first + a[j + 1].first);
      }
      for (int j = 0; j + 1 < a.size(); j++) {
        ans = max(ans, a[j].second * 2);
        ans = max(ans, a[j + 1].second * 2);
        long long v = a[j + 1].first - a[j].first;
        long long x = a[j].second, y = a[j + 1].second;
        if (x > y) swap(x, y);
        if (v > y - x) {
          ans = max(ans, y * 2 + (v - y + x));
        }
      }
    }
  }
  cout << ans / 2;
  if (ans & 1) cout << ".5" << endl;
  return 0;
}
