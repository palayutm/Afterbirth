#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  vector<pair<int, int>> a(1);
  vector<int> b(n + 1);
  b[n] = 1;
  for (int i = 1, t; i <= n; i++) {
    cin >> t;
    b[i - 1] = t;
    if (t == -1) {
      a.push_back({a.back().first + 1, 2 * i - 1});
      a.push_back({a.back().first + 1, 2 * i});
    } else if (t == 0) {
      a.push_back({a.back().first - 1, 2 * i});
    } else {
      a.push_back({a.back().first - 1, 2 * i - 1});
      a.push_back({a.back().first - 1, 2 * i});
    }
  }
  vector<pair<int, int>> st(1, {-1e9, 2 * n + 1});
  int ans = 0;
  for (int i = a.size() - 1; i >= 0; i--) {
    while (a[i].first <= st.back().first) st.pop_back();
    int v = st.back().second - a[i].second;
    if (b[(st.back().second - 1) / 2] == 0) v -= 2;
    else v -= 1;
    ans = max(ans, v);
    st.push_back(a[i]);
  }
  cout << ans * 0.5 << endl;
  return 0;
}
