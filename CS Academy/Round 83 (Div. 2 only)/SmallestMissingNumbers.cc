#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int n, M;
  cin >> n >> M;
  vector<int> a(n, 0);
  long long sum = 0;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  sort(a.begin(), a.end());
  vector<long long> b(n + 1, 0);
  for (int i = 1; i <= n; i++) {
    b[i] = b[i - 1] + a[i - 1];
  }
  long long low = 1, high = 1234567890;
  while (low <= high) {
    int mid = (low + high) / 2;
    int v = upper_bound(a.begin(), a.end(), mid) - a.begin();
    if (mid - v < M) low = mid + 1;
    else high = mid - 1;
  }
  cout << (1LL + low) * low / 2 - b[upper_bound(a.begin(), a.end(), low) - a.begin()] << endl;
  return 0;
}
