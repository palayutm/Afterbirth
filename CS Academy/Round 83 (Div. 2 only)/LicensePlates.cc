#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  string s;
  cin >> s;
  char x = 'a';
  long long ans = 1;
  for (auto c : s) {
    int v = (c == 'd' ? 10 : 26);
    if (c == x) {
      v--;
    }
    ans *= v;
    x = c;
  }
  cout << ans << endl;
  return 0;
}
