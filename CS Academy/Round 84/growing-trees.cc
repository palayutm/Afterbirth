#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, D;
  cin >> n >> D;
  vector<tuple<int, int, int, int>> edges;
  for (int i = 1; i < n; i++) {
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    edges.emplace_back(a, b, c, d);
  }

  vector<vector<tuple<int, int, int>>> g(n);
  for (auto e : edges) {
    long long u, v, c, a;
    tie(u, v, c, a) = e;
    g[u - 1].emplace_back(v - 1, c, a);
    g[v - 1].emplace_back(u - 1, c, a);
  }

  function<long long(int, int, int, long long&)> dfs = [&](int u, int fa, int m, long long& ans) {
    long long d = 0;
    for (auto e : g[u]) {
      long long v, c, a;
      tie(v, c, a) = e;
      if (v != fa) {
        long long dv = dfs(v, u, m, ans);
        ans = max(ans, d + c + m * a + dv);
        d = max(d, c + m * a + dv);        
      }
    }
    return d;
  };

  auto cal = [&](int mid) {
    long long ans = 0;
    dfs(0, -1, mid, ans);
    return ans;
  };
  int low = 0, high = D;
  while (low + 2 < high) {
    int m1 = (low + high) / 2;
    int m2 = (m1 + high) / 2;
    if (cal(m1) <= cal(m2)) high = m2;
    else low = m1;
  }
  int choose = low;
  long long ans = cal(low);
  for (int i = low + 1; i <= high; i++) {
    long long v = cal(i);
    if (v < ans) {
      ans = v;
      choose = i;
    }
  }
  cout << choose << endl << ans << endl;
  return 0;
}
