#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, W;
  cin >> n >> W;
  for (int i = 0, now = 0, st = -1; i < n; i++) {
    int t, v;
    cin >> t >> v;
    if (t == 1) {
      now += v;
      if (now >= W) {
        cout << st + 2 << endl;
        return 0;
      }
    } else {
      now = 0, st = i;
    }
  }
  cout << -1 << endl;
  return 0;
}
