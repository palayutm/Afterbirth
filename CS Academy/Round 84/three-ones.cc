#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n;
  string s;
  cin >> n >> s;
  vector<int> a(n + 1);
  for (int i = 1; i <= n; i++) {
    a[i] = a[i - 1] + s[i - 1] - '0';
  }
  int low = 3, high = n;
  while (low <= high) {
    int mid = (low + high) / 2;
    bool ok = true;
    for (int i = mid; i <= n; i++) {
      ok &= (a[i] - a[i - mid] >= 3);
    }
    if (!ok) low = mid + 1;
    else high = mid - 1;
  }
  cout << low << endl;
  return 0;
}
