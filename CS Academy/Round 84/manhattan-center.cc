#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, K;
  cin >> n >> K;
  vector<pair<int, int>> a(n);
  for (int i = 0; i < n; i++) {
    cin >> a[i].first >> a[i].second;
  }
  sort(a.begin(), a.end());
  auto b = a;
  reverse(b.begin(), b.end());
  auto cal = [](vector<pair<int, int>> a, int K, int xx) {
    vector<long long> d(a.size());
    priority_queue<long long> q;
    long long sum = 0;
    for (int i = 0; i < a.size(); i++) {
      d[i] = sum;
      sum += a[i].first * xx + a[i].second;
      q.push(a[i].first * xx + a[i].second);
      while (q.size() > K) {
        sum -= q.top();
        q.pop();
      }
    }
    return d;
  };
  int k1 = (K - 1) / 2;
  int k2 = K / 2;
  auto v1 = cal(a, k1, -1);
  auto v2 = cal(b, k2, 1);
  long long ans = 1e18;
  for (int i = k1; i + k2 < a.size(); i++) {
    ans = min(ans, v1[i] + v2[a.size() - 1 - i] + a[i].second + 1LL * (k1 - k2) * a[i].first);
  }
  cout << ans << endl;
  return 0;
}
