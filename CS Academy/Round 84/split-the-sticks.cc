#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  long long sum = 0;
  int n;
  cin >> n;
  vector<pair<int, int>> ans;
  set<int> se;
  for (int i = 0, t; i < n; i++) {
    cin >> t;
    sum += t;
    if (se.count(t)) {
      se.erase(t);
    } else {
      se.insert(t);
    }
  }
  if (sum & 1) cout << -1 << endl;
  else {
    while (se.size()) {
      if (se.size() == 1) {
        ans.emplace_back(*se.begin(), *se.begin() / 2);
        se.erase(*se.begin());
      } else {
        int v1 = *se.rbegin(), v2 = *se.begin();
        ans.emplace_back(v1, v2);
        se.erase(v1), se.erase(v2);
        if (se.count(v1 - v2)) {
          se.erase(v1 - v2);
        } else {
          se.insert(v1 - v2);
        }
      }
    }
  }
  cout << ans.size() << endl;
  for (auto x : ans) {
    cout << x.first << " " << x.second << endl;
  }
  return 0;
}
