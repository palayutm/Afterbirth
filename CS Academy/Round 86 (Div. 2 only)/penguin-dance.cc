#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int n, P, T;
  cin >> n >> P >> T;
  int now = 1;
  for (int i = 0; i < T; i++) {
    if (i % 9 >= 4) {
      if (i % 9 == 5) now--;
      else now++;
    }
  }
  if (now + n - 1 >= P && now <= P) {
    cout << P - now + 1 << endl;
  } else {
    cout << -1 << endl;
  }
  return 0;
}
