#include <bits/stdc++.h>

using namespace std;

int f[333][33][33];

int main(int argc, char *argv[]) {
  memset(f, 0x3f, sizeof(f));
  string s;
  cin >> s;
  for (int i = 0; i < 26; i++) {
    for (int j = 0; j < 26; j++) {
      if (i != j) {
        f[1][i][j] = (int)(s[0] != i + 'a') + (int)(s[1] != j + 'a');        
      }
    }
  }
  for (int i = 1; i + 1 < s.size(); i++) {
    for (int j = 0; j < 26; j++) {
      for (int k = 0; k < 26; k++) {
        for (int h = 0; h < 26; h++) {
          if (h != j && h != k) {
            f[i + 1][k][h] = min(f[i + 1][k][h], f[i][j][k] + (s[i + 1] != h + 'a'));
          }
        }
      }
    }
  }
  int ans = 1234567890;
  for (int i = 0; i < 26; i++) {
    for (int j = 0; j < 26; j++) {
      ans = min(ans, f[s.size() - 1][i][j]);
    }
  }
  cout << ans << endl;
  return 0;
}
