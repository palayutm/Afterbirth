#include <bits/stdc++.h>

using namespace std;

const double eps = 1e-6;

int main(int argc, char *argv[]) {
  std::ios_base::sync_with_stdio(false);
  int H, Q;
  cin >> H >> Q;
  vector<pair<int, int>> a;
  for (int i = 0; i < Q; i++) {
    int t, h;
    cin >> t >> h;
    a.emplace_back(t, h);
  }
  a.emplace_back(2e9, 2e9);
  sort(a.begin(), a.end());
  double now = H, T = a[0].first;
  priority_queue<int> q;
  q.push(a[0].second);
  for (int i = 1; i < a.size() && now - eps > 0; i++) {
    while (true) {
      if (now - eps < 0) break;
      if (q.size() == 0) {
        T = a[i].first;
        break;
      }
      double low = T, high = a[i].first;
      bool ok = false;
      for (int it = 0; it < 70; it++) {
        double mid = (low + high) / 2;
        if (now - (mid - T) * q.size() >= q.top()) low = mid;
        else high = mid, ok = true;
      }
      now -= (low - T) * q.size();
      T = low;
      if (ok) q.pop();
      else break;
    }
    q.push(a[i].second);
  }
  if (now - eps > 0) {
    cout << -1 << endl;
  } else {
    cout << setprecision(10) << T << endl;
  }
  return 0;
}
