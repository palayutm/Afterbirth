#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[]) {
  int N, C, S;
  cin >> N >> C >> S;
  vector<int> a(N);
  for (int i = 0; i < N; i++) {
    a[i] = i;
  }
  vector<int> A(N), B(N);
  for (int i = 0; i < N; i++) {
    cin >> A[i] >> B[i];
  }
  int ans = (C - 1) / S + 1;
  do {
    int sum = 0, now = S, T = 0, p = 0;
    while (true) {
      T++;
      sum += now;
      if (sum >= C) {
        ans = min(ans, T);
        break;
      }
      while (p < N && sum >= A[a[p]]) {
        sum -= A[a[p]];
        now += B[a[p]];
        p++;
        ans = min(ans, (C - sum - 1) / now + 1 + T);
      }
    }
  } while (next_permutation(a.begin(), a.end()));
  cout << ans << endl;
  return 0;
}
